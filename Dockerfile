FROM node:latest as builder
ARG arg
RUN mkdir -p /app
COPY ./ ./app
WORKDIR /app
RUN if [ "$arg" = "stage" ]; then rm config/production.json && mv config/stage.json config/production.json && npm i && npm run build; else rm config/production.json && mv config/development.json config/production.json && npm i && npm run build; fi

FROM nginx
RUN mkdir -p /app
WORKDIR /app 
COPY --from=builder /app/web ./web
RUN sed -i 's#/usr/share/nginx/html#/app/web/#g'  /etc/nginx/conf.d/default.conf
RUN sed -i '/error_page   500/i error_page  405     =200 $uri;' /etc/nginx/conf.d/default.conf
RUN service nginx start
CMD ["nginx", "-g", "daemon off;"]
