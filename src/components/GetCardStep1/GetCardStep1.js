import check from "@mixins/check.js"
export default {
    name: 'GetCardStep1',
    mixins: [check],
    data: function () {
        return {
            disabled: true,
            personal: false,
            service: false,
        }
    },
    methods: {
        checkPromocode(code) {
            if (code === 'XLD23S1') {
                this.$parent.promocodeSuccess = true;
                this.$parent.promocodeValue = code;
            } else {
                this.$parent.promocodeSuccess = false;
                this.$parent.promocodeValue = code;
            }
            return

            //узнать метод бэка
            const formData = new FormData();
            formData.append('code', code);
            this.$http.post(`${process.env.APPLICATION_API}/code/check`,  formData)
                .then(status => {
                    this.$parent.promocodeSuccess = true;
                    this.$parent.promocodeValue = code;
                })
                .catch(error => {
                    this.$parent.promocodeSuccess = false;
                    this.$parent.promocodeValue = code;
                })
        },
    },
    watch: {
        personal: function () {
            this.check(['personal', 'service'])
        },
        service: function () {
            this.check(['personal', 'service'])
        }
    }
};
