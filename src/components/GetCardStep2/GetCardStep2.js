import flatpickr from "flatpickr"
import {Russian} from 'flatpickr/dist/l10n/ru.js';
import {english} from 'flatpickr/dist/l10n/default.js';
import {Persian} from 'flatpickr/dist/l10n/fa.js';
import {Arabic} from 'flatpickr/dist/l10n/ar.js';

export default {
    name: 'GetCardStep2',
    data: function () {
        return {
            formHint: {
                ru: 'Заполняйте на английском языке как в загранпаспорте',
                en: 'Заполняйте на английском языке как в загранпаспорте',
                fa: 'Заполняйте на английском языке как в загранпаспорте',
                ar: 'Заполняйте на английском языке как в загранпаспорте',
            },
            title: {
                ru: 'Паспортные данные',
                en: 'Паспортные данные',
                fa: 'Паспортные данные',
                ar: 'Паспортные данные',
            },
            arabicNumbers: ['۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'],
            datepickerLang: null
        }
    },
    created() {
        this.changeLang()
    },
    mounted() {
        this.$parent.initFlatpickr();
    },
    methods: {
        
        removeFile() {
            this.$parent.image = '';
            this.$refs.fileInput.value = '';
            this.$parent.fileInput = '';
        },
        fileChange() {
            const files = this.$refs.fileInput.files[0]
            const fileSize = +(files.size / 1048576).toFixed(2)
            const fileName = files.name

            const allowedExtensions = ['image/png', 'image/jpeg', '.pdf', 'application/pdf']
            const isValid = allowedExtensions.some((item) => item === files.type);

            if(isValid === true) {
                if (fileSize >= 10) {
                    this.$root.openModal('modalBigSize')
                    this.removeFile()
                }
                else {
                    this.$parent.fileInput = files
                    this.$parent.formattedFileName = fileName
                    this.$parent.fileSize = fileSize
                    this.createImage( files);
                }
            }
            else {
                return false
            }
		},

		createImage( file ) {
			this.$parent.file = file;
			// const image = new Image();
			const reader = new FileReader();
			
			reader.onload = ( e ) => {
				this.$parent.image = e.target.result;
			};
            reader.readAsDataURL( file );
		},
		removeImage( ) {
			this.$parent.image = '';
        },
        
        changeLang() {
            switch(this.$root.lang) {
                case 'ru':
                    this.datepickerLang = Russian
                    break;
                case 'en':
                    this.datepickerLang = english
                    break;
                case 'fa':
                    this.datepickerLang = Persian
                    break;
                case 'ar':
                    this.datepickerLang = Arabic
                    break;
                default:
                    this.datepickerLang = english
                    break;
            }
        }
    },
    watch: {
        ["$root.lang"]: function () {
            this.changeLang()
            this.$parent.initFlatpickr()
        },
        // ["$parent.lang"]: function () {
        //     this.changeLang()
        //     this.initFlat()
        // },
    }
}
