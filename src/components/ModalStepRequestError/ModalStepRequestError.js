export default {

    data: function () {
        return {
        }
    },
    props: {
        request: {
            type: Function
        }
    },
    methods: {
        repeatFailedRequest() {
            this.request();
            this.$parent.closeModal();
        }
    }
};
