import {Russian} from 'flatpickr/dist/l10n/ru';
import {english} from 'flatpickr/dist/l10n/default';
import {Persian} from 'flatpickr/dist/l10n/fa';
import {Arabic} from 'flatpickr/dist/l10n/ar';


export default {
    data: function () {
        return {
            title: "Статус карты",
            isValid: true,
            statuses: {
                ACCEPTED: {
                    class: '',
                    ru: 'Принята',
                    en: 'Accepted',
                    fa: 'پذیرفته شد',
                    ar: 'تم الاعتماد',
                },
                IN_BANK: {
                    class: '',
                    ru: 'Обрабатывается банком',
                    en: 'Processing by the bank',
                    fa: 'در حال پردازش توسط بانک ',
                    ar: 'يتم معالجته من قبل البنك',
                },
                DELIVERING: {
                    class: '',
                    ru: 'Карта отправлена',
                    en: 'Карта отправлена',
                    fa: 'کارت ارسال شد',
                    ar: 'تم إرسال البطاقة',
                },
                IN_OFFICE: {
                    class: '',
                    ru: 'Карта доставлена',
                    en: 'Card delivered',
                    fa: 'کارت تحویل داده شد',
                    ar: 'تم تسليم البطاقة ',
                },
                CARD_ACTIVATED: {
                    class: '',
                    ru: 'Карта активирована',
                    en: 'Card activated',
                    fa: 'کارت فعال شد',
                    ar: 'البطاقة مفعلة ',
                }
            },
            number: {
                name: 'number',
                labels: {
                    ru: 'Номер заявки',
                    en: 'Application number',
                    fa: 'شماره درخواست',
                    ar: 'رقم الطلب',
                },
                value: '',
            },
            dateofBirth: {
                name: 'dateofBirth',
                labels: {
                    ru: 'Дата рождения',
                    en: 'Date of birth',
                    fa: 'تاریخ تولد',
                    ar: 'تاريخ الميلاد',
                },
                value: '',
            },
            update: {
                ru: 'Обновлён',
                en: 'Обновлён',
                fa: 'وضعیت',
                ar: 'تم التحديث في ',
            },
            statusTitle: {
                ru: 'Статус заявки №',
                en: 'Status of request №',
                fa: 'وضعیت درخواست شماره',
                ar: 'حالة الطلب رقم ',
            },
            formTitle: {
                ru: 'Узнать статус заявки',
                en: 'Card status',
                fa: 'بررسی وضعیت درخواست',
                ar: 'معرفة حالة الطلب',
            },
            check: {
                ru: 'Проверить',
                en: 'Check',
                fa: 'بررسی',
                ar: 'التحقق',
            },
            checkError: {
                ru: 'Заявка не найдена',
                en: 'Request not found',
                fa: 'درخواست یافت نشد',
                ar: 'طلب غير موجود',
            },
            regexp: '\[0-3][0-9]\\.\[0-1][0-9]\\.\[1-2][09][0-9][0-9]',
            dateUpdate: '',
            resultReady: false,
            startDate: '',
            datepickerLang: null
        }
    },
    created() {
        switch(this.$root.lang) {
            case 'ru':
                this.datepickerLang = Russian;
                break;
            case 'en':
                this.datepickerLang = english;
                break;
            case 'fa':
                this.datepickerLang = Persian;
                break;
            case 'ar':
                this.datepickerLang = Arabic;
                break;
            default:
                this.datepickerLang = english;
                break;
        }
    },
    beforeMount() {
        const char = '0';
        let month =new Date().getMonth() + 1;
        let day = new Date().getDate();
        if (String(month).length === 1) {
            month = char + month;
        }
        if (String(day).length === 1) {
            day = char + day;
        }
        this.startDate = this.dateofBirth.value= `${day}.${month}.${new Date().getFullYear() - 18}`;
    },
    mounted() {
        const currentTime = new Date()
        const params = {
            dateOfBirthMax: new Date(),
            dateOfBirthMin: new Date(),
        }
        params.dateOfBirthMax.setYear(currentTime.getFullYear() - 18)
        params.dateOfBirthMin.setYear(currentTime.getFullYear() - 100)
        flatpickr('#statusDate', {
            allowInput: true,
            dateFormat: 'd.m.Y',
            defaultDate: this.startDate,
            minDate: `${params.dateOfBirthMin.getDate()}-${params.dateOfBirthMin.getMonth() + 1}-${params.dateOfBirthMin.getFullYear()}`,
            maxDate: `${params.dateOfBirthMax.getDate()}-${params.dateOfBirthMax.getMonth() + 1}-${params.dateOfBirthMax.getFullYear()}`,
           
            locale: this.datepickerLang,
            onOpen: function() {
                document.querySelector('.flatpickr-calendar.open').style.zIndex = 99999;
            },
            onClose: function() {
                document.querySelector('.flatpickr-calendar.open').style.zIndex = 500;
            }
        });
    },
    methods: {
        sliceDate(date) {
            if (date.length > 10) {
                let slicedDate = this.dateofBirth.value.slice(0,10);
                this.dateofBirth.value = document.getElementById('statusDate').value  = slicedDate;
            }
            if (date.length === 2 || date.length === 5) {
                let formatedDate = date + '.'
                this.dateofBirth.value = document.getElementById('statusDate').value = formatedDate;
            }
        },
        checkStatus() {
            if(!this.number.value){
                this.isValid = false;
                return;
            } 
            if(!this.isValid ) return;
            let dateForReq = this.formatDateForReq(this.dateofBirth.value)
            this.$http.post(`${process.env.APPLICATION_API}/api/application/state/list`, {
                orderNumber: this.number.value,
                birthday: this.dateForRequest
                // birthday: dateForReq
            }).then((res) => {
                for (let i = 0; i < res.data.length; i++) {
                    let el = res.data[i].status
                    if (this.statuses[el]) {
                        this.statuses[el].class = 'active'
                    }
                }
                this.formatDate(res.data[res.data.length - 1].updated)
                this.resultReady = true
            })
            .catch(error => {
                if (Math.floor(error.response.status / 10) === 40) {
                    this.$root.openModal('modalError', false, this.checkError[this.$root.lang])
                }
                else if (Math.floor(error.response.status / 10) === 50) {
                    this.$root.openModal('modalError', false, ' Проверка не удалась, попробуйте позже')
                }
                else {
                    this.$root.openModal('modalError');
                }
            })

        },
        formatDateForReq(date) {
            if (date !== '') {
                let d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [year, month, day].join('-');
            }
        },
        formatDate(date) {
            let fd = new Date(date)
            let h, m, d, mo, y
            h = (fd.getHours() < 10) ? '0' + fd.getHours() : fd.getHours()
            m = (fd.getMinutes() < 10) ? '0' + fd.getMinutes() : fd.getMinutes()
            d = (fd.getDate() < 10) ? '0' + fd.getDate() : fd.getDate()
            mo = (fd.getMonth() + 1 < 10) ? '0' + (fd.getMonth() + 1) : fd.getMonth() + 1
            y = fd.getFullYear().toString()
            let res = `${h}:${m} ${d}.${mo}.${y[2]}${y[3]}`
            this.dateUpdate = res
        },
        validateDateField(date) {
            if (this.dateofBirth.value !== null) {
                this.dateofBirth.value = new Date(date.getFullYear(), date.getDate() + 1, date.getMonth() + 1) ;
            }
        },
        applyDate(date) {
            setTimeout(()=>{
                this.dateofBirth.value = date;
            }, 1)
        }
    },
    computed: {
        dateForRequest() {
            let date = this.dateofBirth.value;
            return date.split('.').reverse().join('-');
        }
    },
    watch: {
        ["number.value"]: function() {
            if(this.number.value.replace(/[-_]/g,"").length < 12){
                this.isValid = false;
            }else{ 
                this.isValid = true           
            }
        }
    }
};
