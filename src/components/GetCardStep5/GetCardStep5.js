export default {
    components: {},
    name: 'GetCardStep5',
    data: function () {
        return {
            postData: {
                "domicile": this.$parent.inputs.domicile.value,
                "surname": this.$parent.inputs.surname.value,
                "name": this.$parent.inputs.name.value,
                "fathersName": this.$parent.inputs.fathersName.value,
                "dateOfBirth": this.$parent.inputs.dateOfBirth.value.split('.').reverse().join('-'),
                "placeOfBirth": this.$parent.inputs.placeOfBirth.value,
                "gender": this.$parent.inputs.gender.value,
                "passportSerialNumber": this.$parent.inputs.passportSerialNumber.value,
                "dateOfIssue": this.$parent.inputs.dateOfIssue.value.split('.').reverse().join('-'),
                "dateOfExpire": this.$parent.inputs.dateOfExpire.value.split('.').reverse().join('-'),
                "placeOfIssue": this.$parent.inputs.placeOfIssue.value,
                "issuingOfficerName": this.$parent.inputs.issuingOfficerName.value,
                "issuingOfficerPosition": this.$parent.inputs.issuingOfficerPosition.value,
                "operCntPlan": this.$parent.inputs.operCntPlan.value,
                "operAmtPlan": this.$parent.inputs.operAmtPlan.value,
                "moneySources": this.$parent.inputs.moneySources.value.length > 0 ? this.$parent.inputs.moneySources.value : null,
                "activity": (this.$parent.inputs.activity.value !== '' && this.$parent.inputs.activity.optional !== true) ? this.$parent.inputs.activity.value : null,
                "position": (this.$parent.inputs.position.value !== '' && this.$parent.inputs.position.optional !== true) ? this.$parent.inputs.position.value : null, ////массив
                "workExperience": (this.$parent.inputs.workExperience.value !== '' && this.$parent.inputs.workExperience.optional !== true) ? this.$parent.inputs.workExperience.value :null,
                "isProprietor": this.$parent.inputs.isProprietor.value,
                "properties": (this.$parent.inputs.properties.value.length > 0) ? this.$parent.inputs.properties.value : null, ////массив
                "vehicleCost": (this.$parent.inputs.vehicleCost.value !== '' && this.$parent.inputs.vehicleCost.optional !== true) ? this.$parent.inputs.vehicleCost.value : null,
                "email": (this.$parent.inputs.email.value !== '') ? this.$parent.inputs.email.value : null,
                "codeword": this.$parent.inputs.codeword.value,
                "phone": this.$parent.inputs.phone.value,
                "promo": this.$parent.promocodeValue,
                "smsCode": this.$parent.inputs.smsCode.value,
                "phoneVerificationIdentifier": '',
                "homeAddress": {
                    "country": this.$parent.inputs.country.value,
                    "administrativeAreaId": this.$parent.inputs.administrativeAreaId.value.toString(),
                    "subAdministrativeAreaId": this.$parent.inputs.subAdministrativeAreaId.value.toString(),
                    "settlementTypeId": this.$parent.inputs.settlementTypeId.value.toString(),
                    "settlementName": this.$parent.inputs.settlementName.value,
                    "square": (this.$parent.inputs.square.value !== '') ? this.$parent.inputs.square.value  : null,
                    "mainStreet": (this.$parent.inputs.mainStreet.value !== '') ? this.$parent.inputs.mainStreet.value : null,
                    "minorStreet": (this.$parent.inputs.minorStreet.value !== '') ? this.$parent.inputs.minorStreet.value : null,
                    "lane": (this.$parent.inputs.lane.value !== '') ? this.$parent.inputs.lane.value : null,
                    "building": (this.$parent.inputs.building.value !== '') ? this.$parent.inputs.building.value : null,
                    "apartment": (this.$parent.inputs.apartment.value !== '') ? this.$parent.inputs.apartment.value : null,
                    "postalCode":this.$parent.inputs.IdentityNo.value.replace(/\-/, '')

                },
                "agencyId": this.$parent.inputs.agencyId.value, //номер
                "deliveryMethod": "OFFICE",
                "productCode": this.$parent.selectedCard.image.toUpperCase(),
                "isRelative": false, ///откуда берется,
                "relativeType": null ///откуда берется
            },
            inputs: [
                'email',
                'codeword',
                'phone',
                'smsCode',
                'agencyId'
            ],
            
            sendedCode: false,
            smsTimer: 60,
            timerActive: false,
            disableSmsButton: null
        }

    },
    mounted() {
        this.disableSmsButton = this.$parent.inputs.phone.valid !== 'valid' || this.$parent.inputs.phone.length === 0;
    },
    methods: {
        sendForm(){
            const formData = new FormData();
            formData.append('files', this.$parent.file);
            const formJson = this.postData;
            formData.append('application', new Blob([JSON.stringify(formJson)], { type : 'application/json' }))

            this.$http.post(`${process.env.APPLICATION_API}/api/application`, formData).then((response) => {
                let orderNumber = response.data.orderNumber;
                this.$root.openModal('modalSuccess', false, orderNumber);
                this.$parent.step = 0;
                this.$parent.$parent.refreshGetCard();
                window.location.hash = '';
            }).catch(() => {
            });

        },
        formatDate(date) {
            if (date !== '') {
                let d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [year, month, day].join('-');
            }
        },
        sendSms() {
            if (this.timerActive === false && this.$parent.inputs.smsCode.valid !== 'valid') {
                this.$http.get(`${process.env.APPLICATION_API}/api/phone/validate?name=${this.$parent.inputs.name.value}&fathersName=${this.$parent.inputs.fathersName.value}&surname=${this.$parent.inputs.surname.value}&phone=${this.$parent.inputs.phone.value.replace('+','%2B')}`)
                    .then(() => {
                        this.getphoneIdentifier()
                    })
                    .catch(() => {
                        this.$root.openModal('modalError', false, 'К данному номеру телефона уже привязана карта. Используйте другой номер');
                        this.disableSmsButton = false;
                        this.timerActive = false;
                        this.smsTimer = 60;
                    });
                this.timerActive = true;
                this.disableSmsButton = true;
                let timer = setInterval(()=>{
                    this.smsTimer -= 1;
                    if (this.smsTimer < 0) {
                        clearInterval(timer);
                        setTimeout(()=>{
                            this.smsTimer = 60;
                            this.timerActive = false;
                            this.disableSmsButton = false;
                        }, 1)
                    }
                }, 1000);
            }
        },
        getphoneIdentifier() {
            this.$http.post(`${process.env.APPLICATION_API}/api/phone`, {
                phone: this.$parent.inputs.phone.value
            }).then((res) => {
                this.postData.phoneVerificationIdentifier = res.data.identifier
            });
        },
        verifyPhone() {
            this.$http.post(`${process.env.APPLICATION_API}/api/phone/verify`, {
                code: this.$parent.inputs.smsCode.value,
                identifier: this.postData.phoneVerificationIdentifier,
            }).then(() => {
                this.$parent.inputs.smsCode.valid = 'valid';
                this.postData.smsCode = this.$parent.inputs.smsCode.value;
                this.$parent.checkstep('step5');
            }).catch(() => {
                this.$parent.inputs.smsCode.valid = 'unvalid';
                this.$parent.checkstep('step5');
            });

        },
        watchSmsCode() {
            if (this.$parent.inputs.smsCode.value.replace('_', '').length === 5 && this.sendedCode === false) {
                this.verifyPhone();
                this.sendedCode = true
            } else {
                this.sendedCode = false
            }
        },
    },
    watch: {
        ["$parent.inputs.phone.value"]: function () {
            this.postData['phone'] = this.$parent.inputs.phone.value;
            this.disableSmsButton = this.$parent.inputs.phone.valid !== 'valid' || this.$parent.inputs.phone.length === 0 || this.smsTimer !== 60;
            if (this.$parent.inputs.phone.valid === 'valid') {
                this.$parent.inputs.smsCode.valid = '';
                this.$parent.inputs.smsCode.value = '';
                this.$parent.checkstep('step5');
            }
        },
        ["$parent.inputs.email.value"]: function () {
            this.postData['email'] = this.$parent.inputs.email.value;
        },
        ["$parent.inputs.codeword.value"]: function () {
            this.postData['codeword'] = this.$parent.inputs.codeword.value;
        },
        ["$parent.inputs.agencyId.value"]: function () {
            this.postData['agencyId'] = this.$parent.inputs.agencyId.value;
        },
    }
};
