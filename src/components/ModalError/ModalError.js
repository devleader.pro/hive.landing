export default {
    data: function () {
        return {
            defaultError: {
                ru: 'Ошибка',
                en: 'Error',
                fa: 'خطا',
                ar: 'خطا'
            },
            close: {
                ru: 'Закрыть',
                en: 'Close',
                fa: 'بستن',
                ar: 'بستن'
            }
        }
    },
    props: {
        text: {
            default: 'Error'
        }
    },
    computed: {
        formatedText() {
            if (typeof this.text === 'object') return this.defaultError[this.$root.lang];
            else return this.text
        }
    }
};
