import ModalCheckStatus from '@components/ModalCheckStatus/ModalCheckStatus.vue'
import ModalError from '@components/ModalError/ModalError.vue'
import ModalBigSize from '@components/ModalBigSize/ModalBigSize.vue'
import ModalSuccess from '@components/ModalSuccess/ModalSuccess.vue'
import ModalPaymentResult from '@components/ModalPaymentResult/ModalPaymentResult.vue'
import ModalStepRequestError from '@components/ModalStepRequestError/ModalStepRequestError.vue'
export default {
	components: { ModalCheckStatus , ModalError, ModalBigSize, ModalSuccess, ModalStepRequestError, ModalPaymentResult },
	methods: {
		closeModal(evt){
			if( evt.target.className === "overlay" || evt.target.className === "modal-close" || evt.target.className === "modal-close-img" || evt.target.className === "button status-button err" ){
				this.$root.modals = {
					openned: false,
					component: '',
					body: {}
				}
			}
		}
	},
	
};
