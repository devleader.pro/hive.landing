export default {
    data: function () {
        return {
            defaultError: {
                ru: 'Изображение слишком большого размера',
                en: 'Image is too large',
                fa: 'Image is too large',
                ar: 'Image is too large'
            },
            close: {
                ru: 'Закрыть',
                en: 'Close',
                fa: 'بستن',
                ar: 'بستن'
            }
        }
    },
    props: {
        text: {
            default: 'Image is too large'
        }
    },
    computed: {
        formatedText() {
            if (typeof this.text === 'object') return this.defaultError[this.$root.lang];
            else return this.text
        }
    }
};
