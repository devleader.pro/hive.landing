export default {
	name: 'SelectCard',
	data: function() {
		return{
			nameSection: {
				ru: 'Выберите карту',
				en: 'Select a card',
				fa: 'کارت را انتخاب کنید',
				ar: 'اختر البطاقة'
			}, 
			pay: {
				ru: 'оплата за границей<br>и в интернете',
				en: 'Payment abroad<br>and on the Internet',
				fa: 'پرداخت در خارج از کشور و در اینترنت',
				ar: 'الدفع في الخارج و في الانترنيت '
			},
			cash: {
				ru: 'снятие в банкоматах<br>по всему Миру',
				en: 'Cash withdrawal <br> across the globe',
				fa: 'برداشت پول نقد در دستگاه های خودپرداز در سراسر جهان',
				ar: 'السحب من أجهزة الصراف الآلي في جميع أنحاء العالم'
			},
			dbocost: {
				ru: 'дистанционное<br>обслуживание',
				en: 'Online banking',
				fa: 'خدمات از راه دور',
				ar: 'الخدمة عن بعد'
			},
			yearcost: {
				ru: 'годовое<br>обслуживание',
				en: 'Annual fee',
				fa: 'هزینه سالانه',
				ar: 'خدمة سنوية'
			},
			cost: {
				ru: 'выпуск<br>карты',
				en: 'Card <br> issuance',
				fa: 'صدور کارت',
				ar: 'إصدار البطاقة'
			},
			getcard: {
				ru: 'Получить карту',
				en: 'Get a card',
				fa: 'دریافت کارت',
				ar: 'الحصول على بطاقة'
			},
			
		}
	},
	methods:{
		selectCard(card){
			this.$root.selectedCard = card;
            this.$parent.step = 1
			this.$parent.scrollToTop('#getcard');
		}
	}
};
