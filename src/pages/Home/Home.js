import HomeScreen from "@sections/HomeScreen/HomeScreen.vue"
import Tiles from "@sections/Tiles/Tiles.vue"
import Steps from "@sections/Steps/Steps.vue"
import GetCard from "@sections/GetCard/GetCard.vue"
import MobileApp from "@sections/MobileApp/MobileApp.vue"
import Yekpay from "@sections/Yekpay/Yekpay.vue"
import Footer from "@sections/Footer/Footer.vue"
import Copyright from "@sections/Copyright/Copyright.vue"
export default {
    components: {
        HomeScreen,
        Tiles,
        Steps,
        GetCard,
        MobileApp,
        Yekpay,
        Footer,
        Copyright
    },
    data: ()=>({
        reInitGetCard: false
    }),
    methods: {
        refreshGetCard() {
            this.reInitGetCard = true;
            setTimeout(()=>{
                this.reInitGetCard = false;
            }, 10)
        }
    }
};
