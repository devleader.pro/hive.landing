export default {
    name: 'Footer',
    created() {
        this.hashName = location.hash;

    },
    mounted() {
        location.hash = '';
        setTimeout(()=>{
            this.scrollTo(this.hashName);
        }, 200)
    },
    data: function () {
        return {
            products: {
                ru: 'Продукты',
                en: 'PRODUCTS',
                fa: 'محصولات',
                ar: 'المنتجات'
            },
            classic: {
                ru: 'Карта Classic',
                en: 'Classic card',
                fa: 'کارت کلاسیک',
                ar: 'بطاقة Classic'
            },
            gold: {
                ru: 'Карта Gold',
                en: 'Gold card',
                fa: 'کارت گلد',
                ar: 'بطاقة Gold'
            },
            platinum: {
                ru: 'Карта Platinum',
                en: 'Platinum card',
                fa: 'کارت پلاتینیوم',
                ar: 'بطاقة Platinum'
            },
            prepaid: {
                ru: 'Карта Prepaid',
                en: 'Prepaid card',
                fa: 'Карта Prepaid',
                ar: 'Карта Prepaid'
            },
            virtual: {
                ru: 'Карта Virtual',
                en: 'Virtual card',
                fa: 'Карта Virtual',
                ar: 'Карта Virtual'
            },
            contacts: {
                ru: 'Контакты',
                en: 'Contact us',
                fa: 'تماس با ما',
                ar: 'إتصل بنا'
            },
            address: {
                ru: '105066, г. Москва, ул. Нижняя Красносельская, д. 40/12, корп. 20',
                en: '105066, Moscow,Nizhnyaya Krasnoselskaya ul.,40/12, bld. 20',
                fa: '105066، مسکو،خیابان نیژنیایا کراسنوسلسکایا ،پلاک 40/12، ساختمان 20',
                ar: '105066 ، مدينة موسكو ،شارع نيجنايا كراسناسيلسكايا ، رقم البناء 40/12 ، رقم المبنى '
            },
            about: {
                ru: 'О банке',
                en: 'About bank',
                fa: 'درباره بانک',
                ar: 'عن البنك'
            },
            info: {
                ru: 'Общая информация',
                en: 'General information',
                fa: 'اطلاعات کلی',
                ar: 'المعلومات العامة'
            },
            requsits: {
                ru: 'Реквизиты',
                en: 'Bank details',
                fa: 'مشخصات',
                ar: 'البيانات البنكية'
            },
            mobilebank: {
                ru: 'Мобильный банк',
                en: 'MOBILE BANK',
                fa: 'موبایل بانک',
                ar: 'بنك المحمول'
            },
            hashName: '',
           
        }
    },
    methods: {
        scrollTo(hash) {
            this.hashName = hash;
            location.hash = '';
            location.hash = this.hashName;
        },
        selectCard(indx){
            this.$root.selectedCard = indx
        }
    }
};
