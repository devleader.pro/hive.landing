// import scrollTo from '@mixins/scrollTo.js'

export default {
    // mixins: [scrollTo],
    data: function () {
        return {
            title: {
                ru: 'Превосходный<br>онлайн банкинг',
                en: 'Advanced<br>online banking services',
                fa: 'بانکداری<br>آنلاین پیشرفته',
                ar: 'الخدمات المصرفية<br>عبر الإنترنت الممتازة'
            },
            slogan: {
                ru: 'Гибкие условия по международным картам',
                en: 'Flexible conditions for international cards',
                fa: 'شرایط قابل انعطاف جهت کارت های بین المللی',
                ar: 'الشروط المرنة على البطاقات الدولية'
            },
            getCard: {
                ru: 'Получить карту',
                en: 'Get a card',
                fa: 'دریافت کارت',
                ar: 'الحصول على بطاقة'
            },
        }
    },
};
