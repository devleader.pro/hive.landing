import Lang from "@components/Lang/Lang.vue"
import Footer from "@sections/Footer/Footer.vue"
export default {
    components: {
        Lang,
        Footer
    },
    mixins: [],
    data: function () {
        return {
            lang: {
                ru: 'Русский',
                en: 'English',
                fa: 'فارسی',
                ar: 'العربية'
            },
            status: {
                ru: 'Статус заявки',
                en: 'Request status',
                fa: 'وضعیت درخواست',
                ar: 'حالة الطلب'
            },
            dbo: {
                ru: 'Интернет-банк',
                en: 'Online banking',
                fa: 'اینترنت بانک',
                ar: 'بنك الانترنت'
            },
            transparency: false,
            menuIsActive: false,
            currentLanguage: this.$root.lang
        }
    },

    methods: {
        updateScroll() {
            this.transparency = (window.pageYOffset < 200) ? false : true
        },
        changeMenuVisibility() {
            this.menuIsActive = !this.menuIsActive;
            if (this.menuIsActive) {
                this.$refs.headerMenu.classList.add('mobileMenu_active')
            }
            else {
                this.$refs.headerMenu.classList.remove('mobileMenu_active')
            }
        }
    },
    mounted() {
        window.addEventListener('scroll', this.updateScroll);
    },
    watch: {
        $lang() {
            this.menuIsActive = false;
            this.$refs.headerMenu.classList.remove('mobileMenu_active');
        }
    }
};
