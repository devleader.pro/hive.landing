export default {
    name: 'Steps',
    created() {
        if (this.$route.query.authority) {
            this.authorityValue = this.$route.query.authority;
            this.confirmPaymentProcess();
        }
    },
    data: function () {
        return {
            nameSection: {
                ru: 'Пополните карту',
                en: 'Top up card',
                fa: 'واریز پول به کارت',
                ar: 'قم بتعبئة البطاقة'
            },
            yekPay: {
                ru: 'Пополнение осуществляется<br>при поддержке yekpay',
                en: 'TRANSFERS ARE CARRIED OUT<br>WITH THE SUPPORT OF YEKPAY',
                fa: 'انتقالات با پشتیبانی از YekPay انجام شده است',
                ar: 'يتم تنفيد التحويلات بدعم من yekpay'
            },
            methods: {
                ru: 'Пожалуйста выберите способ оплаты:',
                en: 'Please select payment method:',
                fa: 'لطفا، یک روش پرداخت را انتخاب کنید:',
                ar: 'يرجى اختيار طريقة الدفع:'
            },
            shetab: {
                ru: 'Оплатить картой SHETAB',
                en: 'Pay with SHETAB card',
                fa: 'پرداخت با کارت شتاب',
                ar: 'قم بالدفع عن طريق بطاقة SHETAB'
            },
            requsits: {
                ru: 'Сформировать реквизиты для пополнения',
                en: 'Generate payment details for recharge ',
                fa: 'دریافت مشخصات برای واریز وجه',
                ar: 'دریافت مشخصات برای واریز وجه'
            },
            data: {
                ru: 'Данные для пополнения',
                en: 'Details for recharge',
                fa: 'مشخصات برای واریز وجه',
                ar: 'مشخصات برای واریز وجه'
            },
            phoneText: {
                ru: 'Номер телефона держателя карты',
                en: 'Cardholder phone number ',
                fa: 'شماره تلفن دارنده ی کارت',
                ar: 'شماره تلفن دارنده ی کارت'
            },
            ammountText: {
                ru: 'Сумма пополнения, €',
                en: 'Amount, €',
                fa: 'مبلغ واریز، €',
                ar: 'مجموع التعبئة ، €'
            },
            replenish: {
                ru: 'Пополнить',
                en: 'Continue',
                fa: 'واریز پول',
                ar: 'تعبئة'
            },
            phoneValue: '',
            amountValue: '',
            authorityValue: null
        }
    },
    methods: {
        startPaymentProcess(e) {
            e && e.preventDefault();
            this.$http.post(`${process.env.APPLICATION_API}/api/yekpay/startPaymentProcess`, {
                amount: this.amountValue,
                phone: this.phoneValue
            }).then(response => {
                response = response.data;
                if (response.Authority) {
                    window.location.href = `https://gate.yekpay.com/api/payment/start/${response.Authority}`
                    this.authorityValue = response.Authority;
                } else {
                    this.$root.openModal('modalError');
                }
            })
            .catch(error => {
                this.$root.openModal('modalError');
            })
        },
        confirmPaymentProcess() {
            this.$http.post(`${process.env.APPLICATION_API}/api/yekpay/confirmPaymentProcess`, {
                    authority: this.authorityValue
            }).then(response => {
                response = response.data;
                this.$root.openModal('ModalPaymentResult', false, response.Description.split(':')[0])
            })
            
        }
    }
};
