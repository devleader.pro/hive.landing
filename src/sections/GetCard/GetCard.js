import SelectCard from '@components/SelectCard/SelectCard.vue'
import GetCardStep1 from '@components/GetCardStep1/GetCardStep1.vue'
import GetCardStep2 from '@components/GetCardStep2/GetCardStep2.vue'
import GetCardStep3 from '@components/GetCardStep3/GetCardStep3.vue'
import GetCardStep4 from '@components/GetCardStep4/GetCardStep4.vue'
import GetCardStep5 from '@components/GetCardStep5/GetCardStep5.vue'
import { log } from 'util';
let date = new Date()
export default {
    name: 'GetCards',
    components: {
        SelectCard,
        GetCardStep1,
        GetCardStep2,
        GetCardStep3,
        GetCardStep4,
        GetCardStep5
    },
    data: function () {
        return {
            //step1
            arabicNumbers: ['۰','۱','۲','۳','۴','۵','۶','۷','۸','۹'],
            pay: {
                ru: 'оплата за границей<br>и в интернете',
                en: 'Payment abroad<br>and on the Internet',
                fa: 'پرداخت در خارج از کشور و در اینترنت',
                ar: 'الدفع في الخارج و في الانترنيت '
            },
            cash: {
                ru: 'снятие в банкоматах<br>по всему Миру',
                en: 'снятие в банкоматах<br>по всему Миру',
                fa: 'برداشت پول نقد در دستگاه های خودپرداز در سراسر جهان',
                ar: 'السحب من أجهزة الصراف الآلي في جميع أنحاء العالم'
            },
            dbocost: {
                ru: 'дистанционное <br>обслуживание',
                en: 'Online banking',
                fa: 'خدمات از راه دور',
                ar: 'الخدمة عن بعد'
            },
            yearcost: {
                ru: 'годовое <br>обслуживание',
                en: 'Annual fee',
                fa: 'هزینه سالانه',
                ar: 'خدمة سنوية'
            },
            cost: {
                ru: 'выпуск <br>карты',
                en: 'выпуск <br>карты',
                fa: 'صدور کارت',
                ar: 'إصدار البطاقة'
            },
            title: {
                ru: 'выпуск <br>карты',
                en: 'выпуск <br>карты',
                fa: 'صدور کارت',
                ar: 'إصدار البطاقة'
            },
            planningOperations: {
                ru: 'Сведения о планируемых операциях в течение месяца',
                en: 'Information on planned transactions during the month',
                fa: 'اطلاعات در مورد عملیات برنامه ریزی شده طی یک ماه',
                ar: 'معلومات عن العمليات المخطط لها خلال الشهر'
            },
            havePromo: {
                ru: 'У вас есть промокод?',
                en: 'Do you have a promo code?',
                fa: 'یا شما کد تبلیغاتی را دارید؟',
                ar: 'هل لديك رمز ترويجي؟',
            },
            apply: {
                ru: 'Применить',
                en: 'Apply',
                fa: 'تأیید',
                ar: 'للتطبيق',
            },
            promoCode: {
                ru: 'Промокод',
                en: 'Promo code',
                fa: 'کد تبلیغاتی',
                ar: 'الرمز الترويجي',
            },
            sendSMSCode: {
                ru: 'Отправить смс с кодом',
                en: 'Send a text-message with the code ',
                fa: 'ارسال پیامک با کد',
                ar: 'إرسال رسالة قصيرة مع الرمز',
            },
            timer1: {
                ru: 'Повторно получить код подтверждения можно через — ',
                en: 'You can receive the confirmation code again in ',
                fa: ' شما می توانید کد تأیید را پس از ',
                ar: 'من الممكن الحصول على رمز التأكيد مرة أخرى بعد ',
            },
            timer2: {
                ru: ' c',
                en: ' seconds',
                fa: ' ثانیه مجددا دریافت کنید ',
                ar: ' ثانية.',
            },
            sendApp: {
                ru: 'Отправить заявку',
                en: 'Send the application',
                fa: 'ارسال درخواست',
                ar: 'إرسال الطلب',
            },
            conditionals: {
                ru: 'Для продолжения ознакомьтесь с условиями:',
                en: 'To continue, read the terms and conditions',
                fa: 'برای ا دامه، با شرایط آشنا شوید',
                ar: 'للمتابعة ، اقرأ الشروط والأحكام:',
            },
            next: {
                ru: 'Далее',
                en: 'Next',
                fa: 'ادامه',
                ar: 'التالي',
            },
            prev: {
                ru: 'Назад',
                en: 'Back',
                fa: 'برگشت',
                ar: 'إلى الخلف',
            },
            conditional1: {
                ru: 'Я принимаю условия обслуживания Геобанка',
                en: 'I accept terms and conditions of Geobank',
                fa: 'من شرایط  خدمات Geobank را می پذیرم',
                ar: 'أوافق على شروط خدمة غيو بنك',
            },
            conditional2: {
                ru: 'Я даю согласие на обработку персональных данных',
                en: 'I agree for personal data processing',
                fa: 'من شرایط مربوط به پردازش اطلاعات شخصی را می پذیرم',
                ar: 'أنا أعطي موافقتي على معالجة البيانات الشخصية',
            },
            //step2

            showimage: {
                ru: 'Показать, что заполнять',
                en: 'Show what must be filled out here',
                fa: 'نشان دادن تولتیپ',
                ar: 'إظهار ، ما يجب تعبئته'
            },
            addFile: {
                ru: 'Прикрепить скан загранпаспорта',
                en: 'Attach an international passport scan',
                fa: 'کپی گذرنامه را ضمیمه کنید',
                ar: 'إرفاق مسح جواز السفر'
            },
            addFileHint: {
                ru: 'Формат PDF, JPG, PNG, размером до 10 мб ',
                en: 'Format PDF, JPG, PNG, up to 10 mb',
                fa: 'فرمت PDF, JPG, PNG با حداکثر حجم 10 مگابایت',
                ar: 'الاشكال PDF ، JPG ، PNG ، يصل الحجم  إلى 10 ميغابايت'
            },
            addedFile: {
                ru: 'Прикреплено',
                en: 'Attached',
                fa: 'ضمیمه شده است:',
                ar: 'مرفق  : '
            },
            closeimage: {
                ru: 'Скрыть подсказку',
                en: 'Hide the hint',
                fa: 'پنهان کردن تولتیپ',
                ar: 'رقم التعريف'
            },
            step1: {
                ru: 'Карта и промокод',
                en: 'Card and promo code',
                fa: 'کارت و کد تبلیغاتی',
                ar: 'البطاقة والرمز الترويجي'
            },
            fileRule: {
                ru: 'Формат PDF, JPG, PNG, размером до 10 мб',
                en: 'Format PDF, JPG, PNG, up to 10 mb',
                fa: 'فرمت PDF, JPG, PNG با حداکثر حجم 10 مگابایت',
                ar: 'الاشكال PDF ، JPG ، PNG ، يصل الحجم  إلى 10 ميغابايت'
            },
            selectedCard: this.$root.selectedCard,
            inputs: {
                IdentityNo: { ///Не вижу в теле запроса
                    name: 'IdentityNo',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Идентификационный номер',
                        nameEn: 'ID number',
                        nameFa: 'شماره ملی',
                        nameAr: 'رقم التعريف',
                    },
                    disable: false,
                    valid: '',
                    value: '',
                    hints: true,
                    imgHint: 'assets/img/passport-no.png',
                    textHint: {
                        nameRu: 'Идентификационный номер из<br>Национального удостоверения личности',
                        nameEn: 'ID number from the National Identity Card',
                        nameFa: 'شماره ملی که در کارت شناسایی ملی مشخص شده است ',
                        nameAr: 'رقم التعريف من	بطاقة الهوية الوطنية',
                    },
                    openImage: false,

                    regexp: '\[۰۱۲۳۴۵۶۷۸۹0-9]{1}-\[۰۱۲۳۴۵۶۷۸۹0-9]{6}-\[۰۱۲۳۴۵۶۷۸۹0-9]{3}',
                    regexpTest: '[۰۱۲۳۴۵۶۷۸۹]{1}-[۰۱۲۳۴۵۶۷۸۹]{6}-[۰۱۲۳۴۵۶۷۸۹]{3}'
                    // regexp: '\[0-9]{1}\-\[0-9]{6}\-\[0-9]{3}',
                    // regexpTest: '[0-9]{1}\-\[0-9]{6}\-\[0-9]{3}',

                },
                domicile: {
                    name: 'domicile',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Гражданство',
                        nameEn: 'Citizenship',
                        nameFa: 'شهروندی',
                        nameAr: 'الجنسية',
                    },
                    disable: true,
                    valid: 'valid',

                    value: 'IR',
                },
                surname: {
                    name: 'surname',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Surname',
                        nameEn: 'Surname',
                        nameFa: 'Surname',
                        nameAr: 'Surname',
                    },
                    disable: false,
                    valid: '',

                    value: '',
                    regexp: '\[A-Za-z_-]*',
                    regexpTest: '[A-Za-z_-]*'
                },
                name: {
                    name: 'name',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Name',
                        nameEn: 'Name',
                        nameFa: 'Name',
                        nameAr: 'Name',
                    },
                    disable: false,
                    valid: '',

                    value: '',
                    regexp: '\[A-Za-z_-]*',
                    regexpTest: '[A-Za-z_-]*'
                },

                fathersName: {
                    name: 'fathersName',
                    type: 'text',
                    label: true,
                    optional: true,
                    labels: {
                        nameRu: 'Father’s Name',
                        nameEn: 'Father’s Name',
                        nameFa: 'Father’s Name',
                        nameAr: 'Father’s Name',
                    },
                    disable: false,
                    valid: '',

                    value: '',
                    regexp: '\[A-Za-z_-]*',
                    regexpTest: '[A-Za-z_-]*'
                },
                dateOfBirth: {
                    name: 'dateOfBirth',
                    type: 'date',
                    label: true,
                    valid: '',
                    value: '',
                    labels: {
                        nameRu: 'Date of Birth',
                        nameEn: 'Date of Birth',
                        nameFa: 'Date of Birth',
                        nameAr: 'Date of Birth',
                    },
                    regexp: '\([0][1-9]|[123][0-9])\\.(0[1-9]|1[012])\\.([1][9]|[2][0])[0-9][0-9]',
                },
                gender: {
                    name: 'gender',
                    type: 'select',
                    label: true,
                    labels: {
                        nameRu: 'Sex',
                        nameEn: 'Sex',
                        nameFa: 'Sex',
                        nameAr: 'Sex',
                    },

                    options: [{
                            id: 'M',
                            nameRu: 'Male'
                        },
                        {
                            id: 'F',
                            nameRu: 'Female'
                        }
                    ],
                    value: ''
                },
                placeOfBirth: {
                    name: 'placeOfBirth',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Place of Birth',
                        nameEn: 'Place of Birth',
                        nameFa: 'Place of Birth',
                        nameAr: 'Place of Birth',
                    },
                    disable: false,
                    valid: '',

                    value: '',
                    //regexp:  /^[A-Za-z_\-\./\s/]/,
                    regexp: '\[A-Za-z_-\\s]*',
                    regexpTest: '[A-Za-z_-\s]*'
                },
                passportSerialNumber: {
                    name: 'passportSerialNumber',
                    type: 'text',
                    label: true,

                    labels: {
                        nameRu: 'Passport №',
                        nameEn: 'Passport №',
                        nameFa: 'Passport №',
                        nameAr: 'Passport №',
                    },
                    disable: false,
                    valid: '',
                    value: '',
                    regexp: '\[a-zA-Z]{1}\[0-9]{8}',
                    regexpTest: '[a-zA-Z]{1}[0-9]{8}'
                },

                dateOfIssue: {
                    name: 'dateOfIssue',
                    type: 'date',
                    label: true,
                    labels: {
                        nameRu: 'Date of Issue',
                        nameEn: 'Date of Issue',
                        nameFa: 'Date of Issue',
                        nameAr: 'Date of Issue'
                    },
                    disabledDates: {
                        from: new Date(),
                    },
                    startDate: new Date(),
                    value: '',
                    valid: '',
                    regexp: '\[0-3][0-9]\\.\[0-1][0-9]\\.\[1-2][09][0-9][0-9]'

                },
                dateOfExpire: {
                    name: 'dateOfExpire',
                    type: 'date',
                    label: true,
                    labels: {
                        nameRu: 'Date of Expiry',
                        nameEn: 'Date of Expiry',
                        nameFa: 'Date of Expiry',
                        nameAr: 'Date of Expiry',
                    },
                    disabledDates: {
                        to: new Date(date.getFullYear(), date.getMonth() + 3, date.getDate())
                    },
                    startDate: new Date(date.getFullYear(), date.getMonth() + 3, date.getDate()),
                    value: '',
                    valid: '',
                    regexp: '\[0-3][0-9]\\.\[0-1][0-9]\\.\[1-2][09][0-9][0-9]',
                },
                placeOfIssue: {
                    name: 'placeOfIssue',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Place of Issue',
                        nameEn: 'Place of Issue',
                        nameFa: 'Place of Issue',
                        nameAr: 'Place of Issue',
                    },
                    disable: false,
                    valid: '',

                    value: '',

                    hints: true,
                    imgHint: 'assets/img/place-of-issue.png',
                    textHint: {
                        nameRu: 'Форма ввода: английский язык',
                        nameEn: 'Input format: English',
                        nameFa: 'فرمت وارد کردن: زبان انگلیسی',
                        nameAr: 'شكل الإدخال: اللغة الإنجليزية',
                    },
                    openImage: false,

                    regexp: '\[A-Za-z_-\\s]*',
                    regexpTest: '[A-Za-z_-\s]*'

                },
                issuingOfficerName: {
                    name: 'issuingOfficerName',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Name of Issuing officer',
                        nameEn: 'Name of Issuing officer',
                        nameFa: 'Name of Issuing officer',
                        nameAr: 'Name of Issuing officer',
                    },
                    disable: false,
                    valid: '',

                    value: '',
                    //regexp: /^[\w-\.\/s]/, 

                    hints: true,
                    imgHint: 'assets/img/name-of-issuing-officer.png',
                    textHint: {
                        nameRu: 'Форма ввода: английский язык',
                        nameEn: 'Input format: English',
                        nameFa: 'فرمت وارد کردن: زبان انگلیسی',
                        nameAr: 'شكل الإدخال: اللغة الإنجليزية',
                    },
                    openImage: false,
                    regexp: '\[A-Za-z_-\\s]*',
                    regexpTest: '[A-Za-z_-\s]*'

                },
                issuingOfficerPosition: {
                    name: 'issuingOfficerPosition',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Position of Issuing officer',
                        nameEn: 'Position of Issuing officer',
                        nameFa: 'Position of Issuing officer',
                        nameAr: 'Position of Issuing officer',
                    },
                    disable: false,
                    valid: '',

                    value: '',
                    regexp: '\[A-Za-z_-\\s]*',
                    regexpTest: '[A-Za-z_-\s]*',

                    hints: true,
                    imgHint: '/assets/img/position-of-issuing-officer.png',
                    textHint: {
                        nameRu: 'Форма ввода: английский язык',
                        nameEn: 'Input format: English',
                        nameFa: 'فرمت وارد کردن: زبان انگلیسی',
                        nameAr: 'شكل الإدخال: اللغة الإنجليزية',
                    },
                    openImage: false,

                },

                country: {
                    name: 'country',
                    label: true,
                    labels: {
                        nameRu: 'Country',
                        nameEn: 'Country',
                        nameFa: 'Country',
                        nameAr: 'Country',
                    },
                    type: 'text',
                    valid: 'valid',
                    disable: true,
                    value: 'ایران',
                    regexp: '\[\\u0621-\\u0628\\u062A-\\u063A\\u0641-\\u0642\\u0644-\\u0648\\u064E-\\u0651\\u0655\\u067E\\u0686\\u0698\\u06A9\\u06AF\\u06BE\\u06CC\\u0020\\u2000-\\u200F\\u2028-\\u202F\\u060C\\u061B\\u061F\\u0640\\u066A\\u066B\\u066C\\s]*',
                    regexpTest: '[\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC\u0020\u2000-\u200F\u2028-\u202F\u060C\u061B\u061F\u0640\u066A\u066B\u066C\s]*'
                },
                administrativeAreaId: {
                    name: 'administrativeAreaId',
                    type: 'select',
                    label: true,
                    labels: {
                        nameRu: 'Провинция',
                        nameEn: 'Province',
                        nameFa: 'استان',
                        nameAr: 'لمقاطعة',
                    },
                    options: [],
                    openImage: false,
                    value: '',
                },
                subAdministrativeAreaId: {
                    name: 'subAdministrativeAreaId',
                    type: 'select',
                    label: true,
                    disable: true,
                    labels: {
                        nameRu: 'Область',
                        nameEn: 'Region',
                        nameFa: 'شهرستان',
                        nameAr: 'الاقليم',
                    },
                    options: [],
                    openImage: false,
                    value: '',
                },
                settlementTypeId: {
                    name: 'settlementTypeId',
                    type: 'select',
                    label: true,
                    labels: {
                        nameRu: 'Вид населённого пункта',
                        nameEn: 'Type of locality',
                        nameFa: 'نوع',
                        nameAr: 'نوع المركز السكني',
                    },
                    options: [],
                    value: '',
                },
                settlementName: {
                    name: 'settlementName',
                    type: 'text',
                    label: true,
                    valid: '',
                    labels: {
                        nameRu: 'Название населённого пункта',
                        nameEn: 'Name of locality',
                        nameFa: 'نام',
                        nameAr: 'اسم المركز السكني',
                    },
                    value: '',
                    regexp: '\[\\u0621-\\u0628\\u062A-\\u063A\\u0641-\\u0642\\u0644-\\u0648\\u064E-\\u0651\\u0655\\u067E\\u0686\\u0698\\u06A9\\u06AF\\u06BE\\u06CC\\u0020\\u2000-\\u200F\\u2028-\\u202F\\u060C\\u061B\\u061F\\u0640\\u066A\\u066B\\u066C\\sA-Za-z]*',
                    regexpTest: '[\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC\u0020\u2000-\u200F\u2028-\u202F\u060C\u061B\u061F\u0640\u066A\u066B\u066C\sA-Za-z]*'
                },
                square: {
                    name: 'square',
                    type: 'text',
                    valid: '',
                    optional: true,
                    label: true,
                    labels: {
                        nameRu: 'Площадь',
                        nameEn: 'Square',
                        nameFa: 'میدان',
                        nameAr: 'المنطقة',
                    },
                    value: '',
                    regexp: '\[\\u0621-\\u0628\\u062A-\\u063A\\u0641-\\u0642\\u0644-\\u0648\\u064E-\\u0651\\u0655\\u067E\\u0686\\u0698\\u06A9\\u06AF\\u06BE\\u06CC\\u0020\\u2000-\\u200F\\u2028-\\u202F\\u060C\\u061B\\u061F\\u0640\\u066A\\u066B\\u066C\\sA-Za-z]*',
                    regexpTest: '[\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC\u0020\u2000-\u200F\u2028-\u202F\u060C\u061B\u061F\u0640\u066A\u066B\u066C\sA-Za-z]*'
                },
                mainStreet: {
                    name: 'mainStreet',
                    type: 'text',
                    label: true,
                    valid: '',
                    optional: true,
                    labels: {
                        nameRu: 'Главная улица',
                        nameEn: 'Main street',
                        nameFa: 'خیابان اصلی ',
                        nameAr: 'الشارع الرئيسي',
                    },
                    value: '',
                    regexp: '\[\\u0621-\\u0628\\u062A-\\u063A\\u0641-\\u0642\\u0644-\\u0648\\u064E-\\u0651\\u0655\\u067E\\u0686\\u0698\\u06A9\\u06AF\\u06BE\\u06CC\\u0020\\u2000-\\u200F\\u2028-\\u202F\\u060C\\u061B\\u061F\\u0640\\u066A\\u066B\\u066C\\sA-Za-z]*',
                    regexpTest: '[\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC\u0020\u2000-\u200F\u2028-\u202F\u060C\u061B\u061F\u0640\u066A\u066B\u066C\sA-Za-z]*'
                },
                minorStreet: {
                    name: 'minorStreet',
                    type: 'text',
                    valid: '',
                    optional: true,
                    label: true,
                    labels: {
                        nameRu: 'Второстепенная улица',
                        nameEn: 'Secondary street',
                        nameFa: 'خیابان فرعی ',
                        nameAr: 'الشارع الثانوي',
                    },
                    value: '',
                    regexp: '\[\\u0621-\\u0628\\u062A-\\u063A\\u0641-\\u0642\\u0644-\\u0648\\u064E-\\u0651\\u0655\\u067E\\u0686\\u0698\\u06A9\\u06AF\\u06BE\\u06CC\\u0020\\u2000-\\u200F\\u2028-\\u202F\\u060C\\u061B\\u061F\\u0640\\u066A\\u066B\\u066C\\sA-Za-z]*',
                    regexpTest: '[\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC\u0020\u2000-\u200F\u2028-\u202F\u060C\u061B\u061F\u0640\u066A\u066B\u066C\sA-Za-z]*'
                },
                lane: {
                    name: 'lane',
                    type: 'text',
                    label: true,
                    valid: '',
                    optional: true,
                    labels: {
                        nameRu: 'Переулок',
                        nameEn: 'Alley',
                        nameFa: 'کوچه',
                        nameAr: 'جادة',
                    },
                    value: '',
                    regexp: '\[\\u0621-\\u0628\\u062A-\\u063A\\u0641-\\u0642\\u0644-\\u0648\\u064E-\\u0651\\u0655\\u067E\\u0686\\u0698\\u06A9\\u06AF\\u06BE\\u06CC\\u0020\\u2000-\\u200F\\u2028-\\u202F\\u060C\\u061B\\u061F\\u0640\\u066A\\u066B\\u066C\\sA-Za-z]*',
                    regexpTest: '[\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC\u0020\u2000-\u200F\u2028-\u202F\u060C\u061B\u061F\u0640\u066A\u066B\u066C\sA-Za-z]*'
                },
                building: {
                    name: 'building',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Номер или название дома',
                        nameEn: 'Number or name of house',
                        nameFa: 'شماره پلاک خانه یا نام ساختمان ',
                        nameAr: 'رقم أوتسمية المنزل',
                    },
                    value: '',
                    optional: true,

                    regexp: '\[\\u0621-\\u0628\\u062A-\\u063A\\u0641-\\u0642\\u0644-\\u0648\\u064E-\\u0651\\u0655\\u067E\\u0686\\u0698\\u06A9\\u06AF\\u06BE\\u06CC\\u0020\\u2000-\\u200F\\u2028-\\u202F\\u060C\\u061B\\u061F\\u0640\\u066A\\u066B\\u066C\\sA-Za-z0-9]*',
                    regexpTest: '[\u0621-\u0628\u062A-\u063A\u0641-\u0642\u0644-\u0648\u064E-\u0651\u0655\u067E\u0686\u0698\u06A9\u06AF\u06BE\u06CC\u0020\u2000-\u200F\u2028-\u202F\u060C\u061B\u061F\u0640\u066A\u066B\u066C\sA-Za-z0-9]*'
                },
                apartment: {
                    name: 'apartment',
                    type: 'text',
                    label: true,
                    valid: '',
                    optional: true,
                    labels: {
                        nameRu: 'Номер квартиры',
                        nameEn: 'Apartment number',
                        nameFa: 'شماره آپارتمان',
                        nameAr: 'رقم الشقة',
                    },
                    value: '',
                    regexp: '\[\\u06F0-\\u06F9]*[0-9]*',
                    regexpTest: '[\u06F0-\u06F9]*[0-9]*'
                },


                operCntPlan: {
                    name: 'operCntPlan',
                    type: 'select',
                    label: true,
                    labels: {
                        nameRu: 'Количество операций',
                        nameEn: 'Amount of transactions',
                        nameFa: 'تعداد عملیات ',
                        nameAr: 'عدد العمليات',
                    },
                    options: [],
                    valid: '',
                    value: '',
                },
                operAmtPlan: {
                    name: 'operAmtPlan',
                    type: 'select',
                    label: true,
                    labels: {
                        nameRu: 'Общий объём операций',
                        nameEn: 'Total volume of transactions',
                        nameFa: 'مبلغ کل عملیات ',
                        nameAr: 'المبلغ الإجمالي للعمليات',
                    },
                    options: [],
                    valid: '',
                    value: '',
                },
                moneySources: {
                    name: 'moneySources',
                    type: 'checkboxes', //checkbox
                    label: true,
                    optional: false,
                    labels: {
                        nameRu: 'Источники получения денежных средств',
                        nameEn: 'Sources of funds',
                        nameFa: 'منابع دریافت وجوه',
                        nameAr: 'مصادر الأموال المتلقاة',
                    },
                    options: [],
                    valid: '',
                    value: [],
                },
                activity: {
                    name: 'activity',
                    type: 'select',
                    disable: true,
                    label: true,
                    optional: true,
                    labels: {
                        nameRu: 'Сфера деятельности',
                        nameEn: 'Sphere of activity',
                        nameFa: 'زمینه فعالیت ',
                        nameAr: 'نطاق النشاط',
                    },
                    options: [],
                    valid: '',
                    value: ''
                },
                position: {
                    name: 'position',
                    type: 'select',
                    disable: true,
                    label: true,
                    optional: true,
                    labels: {
                        nameRu: 'Должность',
                        nameEn: 'Position',
                        nameFa: 'سمت',
                        nameAr: 'المنصب',
                    },
                    options: [],
                    valid: '',
                    value: '',
                },
                workExperience: {
                    name: 'workExperience',
                    type: 'select',
                    disable: true,
                    label: true,
                    optional: true,
                    labels: {
                        nameRu: 'Стаж работы',
                        nameEn: 'Work experience',
                        nameFa: 'سابقه کار',
                        nameAr: 'خبرة العمل',
                    },
                    options: [],
                    valid: '',
                    value: '',
                },
                isProprietor: {
                    name: 'isProprietor',
                    type: 'radio', //radio
                    label: true,
                    optional: true,
                    labels: {
                        nameRu: 'Являетесь ли вы владельцем или совладельцем недвижимости или транспортного средства?',
                        nameEn: 'Are you an owner or co-owner of a property or vehicle?',
                        nameFa: 'آیا شما مالک یا شریک املاک و مستغلات یا خودرو می باشید؟',
                        nameAr: 'هل أنت المالك أو الشريك للعقار أو المركبة؟',
                    },
                    options: [{
                            id: true,
                            nameRu: 'Да',
                            nameEn: 'Yes',
                            nameFa: 'بله',
                            nameAr: 'نعم'
                        },
                        {
                            id: false,
                            nameRu: 'Нет',
                            nameEn: 'No',
                            nameFa: 'نه',
                            nameAr: 'لا'
                        }
                    ],
                    valid: '',
                    openImage: false,
                    value: false,
                },
                properties: {
                    name: 'properties',
                    type: 'checkboxes', //checkbox
                    label: true,
                    optional: true,
                    disableChildren: true,
                    labels: {
                        nameRu: 'Выберите, чем вы владеете',
                        nameEn: 'Choose what you own',
                        nameFa: 'آنچه را که دارید، انتخاب کنید',
                        nameAr: 'اختر ما تملك',
                    },
                    options: [],
                    valid: '',
                    value: [],
                },

                vehicleCost: {
                    name: 'vehicleCost',
                    type: 'select',
                    label: true,
                    disable: true,
                    optional: true,
                    labels: {
                        nameRu: 'Стоимость ТС',
                        nameEn: 'Cost of the vehicle',
                        nameFa: 'قیمت وسیله نقلیه',
                        nameAr: 'سعر مركبة النقل',
                    },
                    options: [],
                    optionsMain: [],
                    valid: '',
                    value: '',
                },

                phone: {
                    name: 'phone',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Номер мобильного телефона',
                        nameEn: 'Mobile phone number',
                        nameFa: 'شماره تلفن همراه',
                        nameAr: 'رقم الهاتف المحمول',
                    },
                    value: '',
                    regexp: '\\+\[0-9]{0,19}',
                    regexpTest: '\\+\[0-9]{0,19}'
                },

                smsCode: {
                    name: 'smsCode',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Код подтверждения',
                        nameEn: 'Confirmation code',
                        nameFa: 'کد تأیید',
                        nameAr: 'رمز التأكيد',
                    },
                    value: '',
                    regexp: '\[0-9]{5}',
                    regexpTest: '[0-9]{5}'
                },

                email: {
                    name: 'email',
                    type: 'text',
                    label: true,
                    optional: true,
                    labels: {
                        nameRu: 'E-mail',
                        nameEn: 'E-mail',
                        nameFa: 'پست الکترونیک',
                        nameAr: 'البريد الإلكتروني',
                    },
                    textHint: {
                        nameRu: 'Вводите на английском языке',
                        nameEn: 'Enter in English',
                        nameFa: 'به زبان انگلیسی وارد کنید',
                        nameAr: 'أدخل باللغة الإنجليزية',
                    },
                    value: '',
                    // regexp: '(\[a-zA-Z0-9_\\-\\.]+)@(\[a-zA-Z0-9_\\-\\.]+)',
                    // regexpTest: '(\[a-zA-Z0-9_\\-\\.]+)@(\[a-zA-Z0-9_\\-\\.]+)'
                    regexp: '([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+)+(\\.[a-zA-Z0-9]{2,6}){1,3}',
                    regexpTest: '([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+)+(\\.[a-zA-Z0-9]{2,6}){1,3}'

                },
                codeword: {
                    name: 'codeword',
                    type: 'text',
                    label: true,
                    labels: {
                        nameRu: 'Кодовое слово',
                        nameEn: 'Code word',
                        nameFa: 'کلمه کلیدی جهت شناسایی مشتری هنگام مراجعه به بانک از طریق تلفن مورد نیاز است',
                        nameAr: 'كلمة السر',
                    },
                    textHint: {
                        nameRu: 'Кодовое слово нужно для идентификации клиента при его обращении в банк по телефону',
                        nameEn: 'The code word is needed to identify the client when he / she applies to the Bank by phone',
                        nameFa: 'کلمه کلیدی جهت شناسایی مشتری هنگام مراجعه به بانک از طریق تلفن مورد نیاز است',
                        nameAr: 'كلمة السر مطلوبة للتعرف على العميل عندما يتصل بالبنك عبر الهاتف.',
                    },
                    
                    value: '',
                    regexp: '\[a-zA-Z]{10}',
                    regexpTest: '[a-zA-Z]{10}'
                },
                agencyId: {
                    name: 'agencyId',
                    type: 'select',
                    label: true,
                    labels: {
                        nameRu: 'Куда доставить карту?',
                        nameEn: 'Where can we deliver the card?',
                        nameFa: 'آدرس تحویل کارت ',
                        nameAr: 'أين تسليم البطاقة؟',
                    },
                    options: [],
                    openImage: false,
                    value: 0,
                },

            },
            step: 0,

            steps: {
                step1: {
                    mobileTitle: {
                        ru: 'Карта',
                        en: 'Card',
                        fa: 'کارت',
                        ar: 'معلومات الاتصال'
                    },
                    title: {

                        ru: 'Карта и промокод',
                        en: 'Card and promo code',
                        fa: 'کارت و کد تبلیغاتی',
                        ar: 'البطاقة والرمز الترويجي'
                    }
                },
                step2: {
                    mobileTitle: {
                        ru: 'Паспорт',
                        en: 'Passport',
                        fa: 'گذرنامه',
                        ar: 'معلومات الاتصال'
                    },
                    title: {
                        ru: 'Паспортные данные',
                        en: 'Passport data',
                        fa: 'داده های گذرنامه',
                        ar: 'تفاصيل جواز السفر'
                    },
                    hint: {
                        ru: 'Заполняйте на английском языке как в загранпаспорте',
                        en: 'Fill in English as in an international passport',
                        fa: 'به زبان انگلیسی وارد کنید (اطلاعات از گذرنامه)',
                        ar: 'املأ باللغة الإنجليزية كما هو في جواز السفر'
                    },
                    fields: ['domicile', 'surname', 'name', 'fathersName', 'dateOfBirth', 'gender', 'placeOfBirth', 'passportSerialNumber', 'IdentityNo', 'dateOfIssue', 'dateOfExpire', 'placeOfIssue', 'issuingOfficerName', 'issuingOfficerPosition'],
                    valid: false
                },
                step3: {
                    mobileTitle: {
                        ru: 'Адрес',
                        en: 'Address',
                        fa: 'آدرس',
                        ar: 'معلومات الاتصال'
                    },
                    title: {
                        ru: 'Место жительства',
                        en: 'Place of residence',
                        fa: 'محل سکونت ',
                        ar: 'مكان الاقامة'
                    },
                    hint: {
                        ru: 'Заполняйте на арабском',
                        en: 'Please fill out in arabic',
                        fa: 'فرمت وارد کردن: ',
                        ar: 'املأ باللغة الإنجليزية كما هو في جواز السفر'
                    },
                    fields: ['country', 'administrativeAreaId', 'subAdministrativeAreaId', 'settlementTypeId', 'settlementName', 'square', 'mainStreet', 'minorStreet', 'lane', 'building', 'apartment'],
                    valid: false
                },
                step4: {
                    mobileTitle: {
                        ru: 'Анкета',
                        en: 'Questionnaire',
                        fa: 'فرم درخواست',
                        ar: 'معلومات الاتصال'
                    },
                    title: {
                        ru: 'Анкета',
                        en: 'Questionnaire',
                        fa: 'فرم درخواست',
                        ar: 'الاستبيان'
                    },
                    fields: ['operCntPlan', 'operAmtPlan','moneySources', 'activity', 'position', 'workExperience', 'isProprietor', 'properties', 'vehicleCost', ],
                    valid: false
                    // 'operCntPlan', 'operAmtPlan' выделены сразу в верстку
                },
                step5: {
                    mobileTitle: {
                        ru: 'Контакты',
                        en: 'Contact details',
                        fa: 'اطلاعات تماس',
                        ar: 'معلومات الاتصال'
                    },
                    title: {
                        ru: 'Контактная информация',
                        en: 'Contact details',
                        fa: 'اطلاعات مشتری',
                        ar: 'معلومات الاتصال'
                    },
                    fields: ['email', 'codeword', 'phone', 'smsCode', 'agencyId'],
                    valid: false
                },

            },
            image: '',
            formattedFileName: '',
            file: '',
            fileInput: '',
            fileSize: '',
            promocodeValue: '',
            promocodeSuccess: false,
            cards: [
				{
					type: {
						ru: 'Дебетовая карта',
						en: 'Debit card',
						fa: 'کارت نقدی',
						ar: 'بطاقة المدين'
					},
					name: {
						ru: 'Карта Classic',
						en: 'Classic card',
						fa: 'کارت کلاسیک',
						ar: 'بطاقة Classic'
					},
					description: {
						ru: 'Классический сервис. Оптимальный вариант для повседневного использования',
						en: 'Standard service. The best option for day-to-day use',
						fa: 'خدمات کلاسیک بهترین گزینه برای استفاده روزمره',
						ar: 'خدمة كلاسيكية. الخيار الأفضل للاستخدام اليومي'
					},
					pay: '0&nbsp;€',
					cash: '3%',
					dbocost: '3&nbsp;€',
					yearcost: '30&nbsp;€',
					cost: '30&nbsp;€',
					image: 'classic'
				},
				{
					type: {
						ru: 'Дебетовая карта',
						en: 'Debit card',
						fa: 'کارت نقدی',
						ar: 'بطاقة المدين'
					},
					name: {
						ru: 'Карта Gold',
						en: 'Gold card',
						fa: 'کارت گلد',
						ar: 'بطاقة Gold'
					},
					description: {
						ru: 'Премиальный статус. Широкие платежные возможности',
						en: 'Premium status. Wider payment opportunities',
						fa: 'وضعیت امتیاز دارامکان پرداخت گسترده',
						ar: 'حالة ممتازة إمكانيات المدفوعات الواسعة'
					},
					pay: '0&nbsp;€',
					cash: '3%',
					dbocost: '3&nbsp;€',
					yearcost: '60&nbsp;€',
					cost: '30&nbsp;€',
					image: 'gold'
				},
				{
					type: {
						ru: 'Дебетовая карта',
						en: 'Debit card',
						fa: 'کارت نقدی',
						ar: 'بطاقة المدين'
					},
					name: {
						ru: 'Карта Platinum',
						en: 'Platinum card',
						fa: 'کارت پلاتینیوم',
						ar: 'بطاقة Platinum'
					},
					description: {
						ru: 'Платиновые преимущества. Бесплатный мобильный банк и СМС-оповещения',
						en: 'Extra advantages. Mobile banking and SMS-informing services are complimentary',
						fa: 'مزایای خاص ',
						ar: 'ميزات بلاتينية . بنك المحمول المجاني  مجاني وتنبيهات الرسائل القصيرة'
					},
					pay: '0&nbsp;€',
					cash: '3%',
					dbocost: '0&nbsp;€',
					yearcost: '100&nbsp;€',
					cost: '30&nbsp;€',
					image: 'platinum'
				},
				// {
				// 	type: {
				// 		ru: 'Предоплаченная карта',
				// 		en: 'Предоплаченная карта',
				// 		fa: 'کارت بی نام',
				// 		ar: 'البطاقة المدفوعة سابقا '
				// 	},
				// 	name: {
				// 		ru: 'Prepaid Card',
				// 		en: 'Prepaid Card',
				// 		fa: 'Prepaid Card',
				// 		ar: 'Prepaid Card'
				// 	},
				// 	description: {
				// 		ru: 'Международная банковская карта в подарок',
				// 		en: 'Международная банковская карта в подарок',
				// 		fa: 'کارت بانکی بین المللی به عنوان هدیه',
				// 		ar: 'بطاقة بنكية عالمية كهدية .'
				// 	},
				// 	pay: '0&nbsp;€',
				// 	cash: '3%',
				// 	dbocost: '3&nbsp;€',
				// 	yearcost: '30&nbsp;€',
				// 	cost: '30&nbsp;€',
				// 	image: 'prepaid'
				// },
				// {
				// 	type: {
				// 		ru: 'Виртуальная карта',
				// 		en: 'Виртуальная карта',
				// 		fa: 'کارت مجازی',
				// 		ar: 'البطاقة الافتراطية '
				// 	},
				// 	name: {
				// 		ru: 'Virtual Card',
				// 		en: 'Virtual Card',
				// 		fa: 'Virtual Card',
				// 		ar: 'Virtual Card'
				// 	},
				// 	description: {
				// 		ru: 'Моментальный выпуск. Безопасность и удобство онлайн-платежей',
				// 		en: 'Моментальный выпуск. Безопасность и удобство онлайн-платежей',
				// 		fa: 'صدور فوری امنیت و پرداخت آنلاین آسان',
				// 		ar: 'إصدارفوري. أمن وملائمة المدفوعات عبر الإنترنت'
				// 	},
				// 	pay: '0&nbsp;€',
				// 	cash: '—',
				// 	dbocost: '0&nbsp;€',
				// 	yearcost: '10&nbsp;€',
				// 	cost: '10&nbsp;€',
				// 	image: 'virtual'
				// },
			],
        }
    },
    methods: {
        getDictionary() {
            const dictonaries = [

                {
                    name: 'settlementTypeId',
                    dict: 'settlementtype'
                },
                {
                    name: 'operCntPlan',
                    dict: 'numplanoperations'
                },
                {
                    name: 'operAmtPlan',
                    dict: 'sumplanoperations'
                },
                {
                    name: 'moneySources',
                    dict: 'moneysources'
                },
                {
                    name: 'activity',
                    dict: 'activity'
                },
                {
                    name: 'position',
                    dict: 'position'
                },
                {
                    name: 'workExperience',
                    dict: 'workexperience'
                },
                {
                    name: 'properties',
                    dict: 'ownerproperties'
                },
                {
                    name: 'vehicleCost',
                    dict: 'vehiclecost '
                },
                {
                    name: 'agencyId',
                    dict: 'agency '
                },
            ]
            for (let i = 0; i < dictonaries.length; i++) {
                const el = dictonaries[i];
                this.$http.get(`${process.env.DICTIONARY_API}/api/v1/dictionary/${el.dict}`).then((res) => {
                    let data = res.data
                    for (let o = 0; o < data.length; o++) {
                        let labels = data[o]
                        labels.names = {}
                        labels.names.nameEn = labels.nameEn
                        labels.names.nameRu = labels.nameRu
                        labels.names.nameFa = labels.nameFa
                        labels.names.nameAr = labels.nameRu
                        this.inputs[el.name].options.push(labels);
                        if(el.name == "vehicleCost"){ 
                            this.inputs[el.name].optionsMain.push(labels);
                        }
                    }
                }).catch(function (error) {
                    // this.$root.modals.openned = true;
                    // this.$root.modals.component = 'NetworkError';
                    // this.$root.modals.body = error;
                    this.$root.openModal('modalStepRequestError', this.getDictionary);
                });

            }

        },
        getAreas() {
            this.$http.get(`${process.env.DICTIONARY_API}/api/v1/dictionary/adminarea`).then((res) => {
                this.inputs.administrativeAreaId.options = res.data;
            }).catch(function (error) {
                this.$root.openModal('ModalStepRequestError', this.getAreas);
            });
        },
        getSubAreas(id) {
            this.$http.get(`${process.env.DICTIONARY_API}/api/v1/dictionary/adminarea/${id}`).then((res) => {
                this.inputs.subAdministrativeAreaId.options = res.data;
            }).catch(function (error) {
                this.$root.openModal('ModalStepRequestError', this.getSubAreas);
            });
        },
        convertLangToForm() {
            return 'name' + this.$root.lang.charAt(0).toUpperCase() + this.$root.lang.slice(1);
        },
        validTextField(field) {
            if( this.inputs[field].regexpTest ) {
                let regexp = new RegExp(this.inputs[field].regexpTest);
                let regexp1 = new RegExp(this.inputs[field].regexpTest.replace(/-/g, ''))
                let value = this.inputs[field].value.replace(/\s/g, '');
                if( this.inputs[field].value.replace(/\s/g, '') !== '' && value !== '' && (regexp.test(value) || regexp1.test(value) ) ){
                    this.inputs[field].valid = 'valid';
                } else {
                    if (this.inputs[field].optional !== true || this.inputs[field].value !== '')  this.inputs[field].valid = 'unvalid';
                    else this.inputs[field].valid = '';
                }
                if (field === 'email' && this.inputs.email.value.length !== 0) {
                    let arrayEmail = this.inputs[field].value.split('@')[1].split('.');
                    for (let i = 0; i < arrayEmail.length - 1; i++) {
                        if (arrayEmail[i].length < 1) {
                            this.inputs[field].valid = 'unvalid';
                            break;
                        }
                    }
                    if (arrayEmail[arrayEmail.length - 1].length < 2) {
                        this.inputs[field].valid = 'unvalid';
                    }
                }
                if(field === 'email' && this.inputs.email.value.length === 0) {
                    this.inputs[field].valid = 'unvalid'
                }
            }
            this.checkstep(`step${this.step}`)
        },
        validSelectedValue(field) {
            //this.inputs[field].value - убрал это условие, так как он не давал сабмит, если
            //пользователь не является владельцем ТС или недвижимости
            this.inputs[field].valid = (this.inputs[field].value !== '') ? 'valid' : 'unvalid';
            this.checkstep(`step${this.step}`)
        },
        formatDate(date) {
            if (date != '') {
                let d = new Date(date),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();

                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;

                return [year, month, day].join('-');
            }
        },
        checkstep(step) {
            let unvalid = 0;
            for (let i = 0; i < this.steps[step].fields.length; i++) {
                const el = this.steps[step].fields[i];
                if( (this.inputs[el].valid !== 'valid' && this.inputs[el].optional !== true )
                    || (this.inputs[el].optional === true && this.inputs[el].value !== '' && this.inputs[el].valid !== 'valid')
                ){
                    if (el !== 'isProprietor' && this.inputs[el].optional !== true ){
                        unvalid++
                    }

                }
            }
            if (unvalid !== 0) {
                this.steps[step].valid = false;
            } else {
                if (this.step === 2) {
                    if (this.$children[0].$refs.fileInput.value !== '') {
                        this.steps[step].valid = true;
                    }
                    else {
                        this.steps[step].valid = false;
                    }
                }
                else {
                    this.steps[step].valid = true;
                }

            }
        },
        scrollToTop(hash) {
            this.hashName = hash;
            location.hash = '';
            location.hash = this.hashName;
        },
        goToNextStep(hash) {
            this.step++;
            this.scrollToTop(hash);
        },
        goToPrevStep(hash) {
            this.step--;
            this.scrollToTop(hash);
        },
        initFlatpickr(){
            const currentTime = new Date()
            const params = {
                dateOfBirthMax: new Date(),
                dateOfBirthMin: new Date(),
                dateOfIssueMax: new Date(),
                dateOfIssueMin: new Date(),
                dateOfExpireMax: new Date(),
                dateOfExpireMin: new Date(),

            }
            params.dateOfBirthMax.setYear(currentTime.getFullYear() - 18)
            params.dateOfBirthMin.setYear(currentTime.getFullYear() - 100)
            params.dateOfExpireMin.setMonth(currentTime.getMonth() + 3)
            params.dateOfExpireMax.setYear(params.dateOfIssueMin.getFullYear() + 100)
            const inputs = this.inputs
            flatpickr('#dateOfBirth', {
                altForamt: 'd.m.Y',
                allowInput: true,
                dateFormat: 'd-m-Y',
                defaultDate: (inputs.dateOfBirth.value && inputs.dateOfBirth.value != '') ? inputs.dateOfBirth.value :`(${params.dateOfBirthMax.getDate()}-${params.dateOfBirthMax.getMonth() + 1}-${params.dateOfBirthMax.getFullYear()}`,
                minDate: `${params.dateOfBirthMin.getDate()}-${params.dateOfBirthMin.getMonth() + 1}-${params.dateOfBirthMin.getFullYear()}`,
                maxDate: `${params.dateOfBirthMax.getDate()}-${params.dateOfBirthMax.getMonth() + 1}-${params.dateOfBirthMax.getFullYear()}`,
                locale: this.datepickerLang,
                onOpen: ()=>{
                    if (this.inputs.dateOfBirth.value === '') {
                        document.getElementById("dateOfBirth").value = '';
                    }
                    document.getElementById("dateOfBirth").style.opacity = '1';
                }
            });
            flatpickr('#dateOfIssue', {
                allowInput: true,
                dateFormat: 'd-m-Y',
                defaultDate: (inputs.dateOfIssue.value && inputs.dateOfIssue.value != '') ? inputs.dateOfIssue.value : `${params.dateOfIssueMin.getDate()}-${params.dateOfIssueMin.getMonth() + 1}-${params.dateOfIssueMin.getFullYear()}`,
                minDate: (inputs.dateOfBirth.value && inputs.dateOfBirth.value != '') ? inputs.dateOfBirth.value : `${params.dateOfIssueMin.getDate()}-${params.dateOfIssueMin.getMonth() + 1}-${params.dateOfIssueMin.getFullYear()}`,
                maxDate: `${params.dateOfIssueMax.getDate()}-${params.dateOfIssueMax.getMonth() + 1}-${params.dateOfIssueMax.getFullYear()}`,
               
                locale: this.datepickerLang,
                onOpen: ()=>{
                    if (this.inputs.dateOfIssue.value === '') {
                        document.getElementById("dateOfIssue").value = '';
                    }
                    document.getElementById("dateOfIssue").style.opacity = '1';
                }
            });
            flatpickr('#dateOfExpire', {
                allowInput: true,
                dateFormat: 'd-m-Y',
                defaultDate: (inputs.dateOfExpire.value && inputs.dateOfExpire.value != '') ? inputs.dateOfExpire.value : `${params.dateOfExpireMin.getDate()}-${params.dateOfExpireMin.getMonth() + 1}-${params.dateOfExpireMin.getFullYear()}`,
                minDate: `${params.dateOfExpireMin.getDate()}-${params.dateOfExpireMin.getMonth() + 1}-${params.dateOfExpireMin.getFullYear()}`,
                maxDate: `${params.dateOfExpireMax.getDate()}-${params.dateOfExpireMax.getMonth() + 1}-${params.dateOfExpireMax.getFullYear()}`,
               
                locale: this.datepickerLang,
                onOpen: ()=>{
                    if (this.inputs.dateOfExpire.value === '') {
                        document.getElementById("dateOfExpire").value = '';
                    }
                    document.getElementById("dateOfExpire").style.opacity = '1';
                }
            });
           
        },
        handleIdentityNo(event){
            event.preventDefault();
            
            let key  = event.key
            if(/[0-9]/.test(key) && key.length == 1 ){
                this.inputs.IdentityNo.value += this.arabicNumbers[key];
            this.validTextField('IdentityNo')
        }
        }
    },
    mounted() {
        this.getDictionary();
        this.convertLangToForm();
        this.getAreas();
        this.inputs.properties.disableChildren = this.inputs.vehicleCost.disable = !this.inputs.isProprietor.value;
    },
    watch: {
        //step2
        ["inputs.surname.value"]: function () {
            this.validTextField('surname');
        },
        ["inputs.name.value"]: function () {
            this.validTextField('name');
        },
        ["inputs.fathersName.value"]: function () {
            this.validTextField('fathersName');
        },
        ["inputs.placeOfBirth.value"]: function () {
            this.validTextField('placeOfBirth');
        },
        ["inputs.passportSerialNumber.value"]: function () {
            this.validTextField('passportSerialNumber');
        },
        ["inputs.issuingOfficerName.value"]: function () {
            this.validTextField('issuingOfficerName');
        },
        ["inputs.issuingOfficerPosition.value"]: function () {
            this.validTextField('issuingOfficerPosition');
        },
        ["inputs.dateOfBirth.value"]: function () {
            const input = this.inputs.dateOfBirth
            let cleanDate = input.value.replace(/_/g, '')
            if( cleanDate.length === 10){
                this.initFlatpickr()
                let dateArray = input.value.split('.');
                let valueDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
                let checkDateStart = new Date(new Date().getFullYear() - 100,new Date().getMonth() + 1, new Date().getDate());
                let checkDateEnd = new Date(new Date().getFullYear() - 18,new Date().getMonth() + 1, new Date().getDate());
                if ( cleanDate.length == 10 && (valueDate > checkDateEnd || valueDate < checkDateStart) ){
                    input.valid = 'unvalid'
                    input.value = ''
                } else {
                    this.inputs.dateOfIssue.startDate = valueDate
                    
                    this.validSelectedValue('dateOfBirth');
                    this.checkstep('step2');
                }
            } else {
                input.valid = 'unvalid'
            }
        },
        ["inputs.dateOfIssue.value"]: function () {
            const input = this.inputs.dateOfIssue
            let cleanDate = input.value.replace(/_/g, '')
            if(cleanDate.length === 10){
                this.initFlatpickr()
                let dateArray = input.value.split('.');
                let valueDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
                let checkDateStart = this.inputs.dateOfIssue.startDate
                let checkDateEnd = new Date(new Date().getFullYear(),new Date().getMonth() +1, new Date().getDate());
                if ( cleanDate.length == 10 && (valueDate > checkDateEnd || valueDate < checkDateStart) ){
                    input.valid = 'unvalid'
                    input.value = ''
                } else {
                    
                    this.validSelectedValue('dateOfIssue');
                    this.checkstep('step2');
                }
            } else {
                input.valid = 'unvalid'
            }
        },
        ["inputs.dateOfExpire.value"]: function () {
            const input = this.inputs.dateOfExpire
            let cleanDate = input.value.replace(/_/g, '')
            if(cleanDate.length === 10){
                this.initFlatpickr()
                let dateArray = input.value.split('.');
                let valueDate = new Date(dateArray[2], dateArray[1], dateArray[0]);
                let checkDateStart = new Date(new Date().getFullYear(),new Date().getMonth() +4, new Date().getDate());
                let checkDateEnd = new Date(new Date().getFullYear() + 100,new Date().getMonth() +1, new Date().getDate());
                if ( cleanDate.length == 10 && (valueDate > checkDateEnd || valueDate < checkDateStart) ){
                    input.valid = 'unvalid'
                    input.value = ''
                } else {
                    
                    this.validSelectedValue('dateOfExpire');
                    this.checkstep('step2');
                }
            } else {
                input.valid = 'unvalid'
            }
            
        },
        ["inputs.placeOfIssue.value"]: function () {
            this.validTextField('placeOfIssue')
        },
        ["inputs.gender.value"]: function () {
            this.validSelectedValue('gender')
        },
        ["fileInput"]: function () {
            this.checkstep(`step2`)
        },
        //step3
        ["inputs.administrativeAreaId.value"]: function () {
            this.inputs.subAdministrativeAreaId.disable = this.inputs.administrativeAreaId.value === '';
            this.inputs.subAdministrativeAreaId.value = [];
            this.inputs.subAdministrativeAreaId.options =  [{
                id: 33,
                nameEn: "",
                nameFa: "",
                nameRu: "",
                order: 40
            }];
            this.validSelectedValue('administrativeAreaId');
            this.getSubAreas(this.inputs.administrativeAreaId.value);
        },
        ["inputs.subAdministrativeAreaId.value"]: function () {
            this.inputs.subAdministrativeAreaId.valid = null;
            this.validSelectedValue('subAdministrativeAreaId');
            if(this.inputs.subAdministrativeAreaId.options[0].id == 33){
                this.inputs.subAdministrativeAreaId.valid = null;
            }
            
        },
        ["inputs.settlementTypeId.value"]: function () {
            this.validSelectedValue('settlementTypeId')
        },
        ["inputs.settlementName.value"]: function () {
            this.validTextField('settlementName')
        },
        ["inputs.square.value"]: function () {
            this.validTextField('square')
        },
        ["inputs.mainStreet.value"]: function () {
            this.validTextField('mainStreet')
        },
        ["inputs.minorStreet.value"]: function () {
            this.validTextField('minorStreet')
        },
        ["inputs.lane.value"]: function () {
            this.validTextField('lane')
        },
        ["inputs.building.value"]: function () {
            this.validTextField('building')
        },
        ["inputs.apartment.value"]: function () {
            this.validTextField('apartment')
        },
        //step4
        ["inputs.operCntPlan.value"]: function () {
            this.validSelectedValue('operCntPlan')
        },
        ["inputs.operAmtPlan.value"]: function () {
            this.validSelectedValue('operAmtPlan')
        },
        ["inputs.moneySources.value"]: function () {

            if (this.inputs.moneySources.value.indexOf("SALARY") !== -1) {
                this.inputs.activity.disable = this.inputs.position.disable = this.inputs.workExperience.disable = false;
                this.inputs.activity.optional = this.inputs.position.optional = this.inputs.workExperience.optional = false;
                if (this.inputs.activity.value !== '') this.validSelectedValue('activity');
                if (this.inputs.position.value !== '') this.validSelectedValue('position');
                if (this.inputs.workExperience.value !== '') this.validSelectedValue('workExperience');
            }
            else {
                this.inputs.activity.disable = this.inputs.position.disable = this.inputs.workExperience.disable = true;
                this.inputs.activity.optional = this.inputs.position.optional = this.inputs.workExperience.optional = true;
                this.inputs.activity.valid = this.inputs.position.valid = this.inputs.workExperience.valid = '';
            }
            this.validSelectedValue('moneySources')
        },
        ["inputs.isProprietor.value"]: function () {
    
            if(this.inputs.isProprietor.value){
                this.inputs.vehicleCost.optionsMain = this.inputs.vehicleCost.options;
            }else{
                this.inputs.vehicleCost.disable = true;
                this.inputs.properties.value = [];

               this.inputs.vehicleCost.optionsMain = [{
                   id: "NULL",
                   nameEn: "",
                   nameFa: "",
                   nameRu: "",
                   names:{ 
                       nameAr: "",
                       nameEn: "",
                       nameFa: "",
                       nameRu: ""
                   },
                   order: 0
               }]; 
            }
        
            this.inputs.vehicleCost.valid = '';
            this.inputs.properties.disableChildren = !this.inputs.isProprietor.value
            this.inputs.properties.optional = !this.inputs.isProprietor.value
            this.validSelectedValue('isProprietor');
        },
        ["inputs.activity.value"]: function () {
            this.validSelectedValue('activity')
        },
        ["inputs.position.value"]: function () {
            this.validSelectedValue('position')
        },
        ["inputs.workExperience.value"]: function () {
            this.validSelectedValue('workExperience')
        },
        ["inputs.vehicleCost.value"]: function () {
            this.validSelectedValue('vehicleCost')
        },
        ["inputs.properties.value"]: function () {
            
            if (this.inputs.properties.value.indexOf("CAR_OWNER") !== -1 || this.inputs.properties.value.indexOf("VEHICLE_OWNER") !== -1) {
                this.inputs.vehicleCost.disable = this.inputs.vehicleCost.optional = false;
                if (this.inputs.vehicleCost.value !== '') this.validSelectedValue('vehicleCost');
            }
            else {
                this.inputs.vehicleCost.disable = this.inputs.vehicleCost.optional = true;
                this.inputs.vehicleCost.valid = '';
            }
            this.validSelectedValue('properties')
        },
        //step5
        ["inputs.phone.value"]: function () {
            this.validTextField('phone');
        },
        ["inputs.email.value"]: function () {
            this.validTextField('email')
        },
        ["inputs.codeword.value"]: function () {
            this.validTextField('codeword')
        },
        ["inputs.agencyId.value"]: function () {
            this.validSelectedValue('agencyId')
        },
        // ["inputs.smsCode.value"]: function () {
        //     this.checkstep('step5');
        // },
        ["$root.selectedCard"]: function () {
            this.selectedCard = this.cards[this.$root.selectedCard]
            this.step = 1
            this.scrollToTop('#getcard')
        },
    }
};
