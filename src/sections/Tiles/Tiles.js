export default {
    name: 'Tiles',
    data: function () {
        return {
            nameSection: {
                ru: 'Возможности',
                en: 'Our capabilities',
                fa: 'امکانات',
                ar: 'الإمكانيات المتاحة'
            },
            tiles: [{
                    ru: {
                        title: 'Совершайте онлайн-покупки',
                        description: 'Удобная оплата товаров и услуг в сети Интернет'
                    },
                    en: {
                        title: 'Make online purchases',
                        description: 'Convenient online payment options for goods and services '
                    },
                    fa: {
                        title: 'خرید آنلاین',
                        description: 'پرداخت آسان کالاها و خدمات در اینترنت '
                    },
                    ar: {
                        title: 'قم بتنفيذ عمليات الشراء عبر الإنترنت',
                        description: 'الدفع المريح للسلع والخدمات على شبكة الإنترنت'
                    },
                    img: 'tile-shopping'
                },
                {
                    ru: {
                        title: 'Рассчитывайтесь  картой за границей',
                        description: 'Отсутствие комиссий при оплате в торговых точках во время путешествий'
                    },
                    en: {
                        title: 'Pay with your card abroad',
                        description: 'No service fee is charged for transactions performed in points of sales while travelling abroad'
                    },
                    fa: {
                        title: 'پرداخت با کارت خود در خارج از کشور',
                        description: 'پرداخت بدون کارمزد در فروشگاه ها طی سفر'
                    },
                    ar: {
                        title: 'قم بالدفع عن طريق البطاقة في الخارج',
                        description: 'لا رسوم عند الدفع في منافذ البيع عند السفر'
                    },
                    img: 'tile-map'
                },
                {
                    ru: {
                        title: 'Управляйте наличными средствами',
                        description: 'Снятие наличных с карты в Банкоматах по всему миру'
                    },
                    en: {
                        title: 'Manage cash',
                        description: 'Cash withdrawal in ATMs across the globe'
                    },
                    fa: {
                        title: 'مدیریت پول نقد',
                        description: 'برداشت و واریز پول نقد در دستگاه خودپرداز در سراسر جهان'
                    },
                    ar: {
                        title: 'قم بإدارة الأموال النقدية',
                        description: 'السحوبات النقدية من البطاقة في أجهزة الصراف الآلي في جميع أنحاء العالم'
                    },
                    img: 'tile-money'
                },
                {
                    ru: {
                        title: 'Используйте мобильный банк',
                        description: 'Мгновенные переводы между картами, детальные выписки'
                    },
                    en: {
                        title: 'Use mobile bank',
                        description: 'Instant card-to-card transfers; detailed account statements'
                    },
                    fa: {
                        title: 'استفاده از موبایل بانک',
                        description: 'انتقال فوری بین کارت ها، صورتحساب ها به همراه جزئیات'
                    },
                    ar: {
                        title: 'استخدم بنك الهاتف المحمول',
                        description: 'التحويلات الفورية بين البطاقات والبيانات التفصيلية'
                    },
                    img: 'tile-phone'
                }
            ]
        }
    },
};
