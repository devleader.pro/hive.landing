export default {
    name: 'Steps',
    data: function () {
        return {
            nameSection: {
                ru: 'Получить карту просто',
                en: 'Getting a card is easier than you may think',
                fa: 'کارت به آسانی قابل دریافت می باشد',
                ar: 'الحصول على بطاقة أمر بسيط'
            },
            steps: [{
                    ru: {
                        title: 'Выберите карту',
                        description: 'Hive представляет три вида карт: дебетовые, виртуальные и предоплаченные '
                    },
                    en: {
                        title: 'Choose a card',
                        description: 'Hive issues three types of cards: debit, virtual and prepaid cards.'
                    },
                    fa: {
                        title: 'کارت را انتخاب کنید',
                        description: 'Hive سه نوع کارت را پیشنهاد می کند:کارت نقدی، کارت مجازی و کارت بی نام'
                    },
                    ar: {
                        title: 'اختر البطاقة',
                        description: 'يقدم Hive ثلاثة أنواع من البطاقات: بطاقات المدين ، و البطاقات الافتراضية ، و البطاقات المدفوعة مسبق'
                    },
                    img: '01'
                },
                {
                    ru: {
                        title: 'Заполните анкету',
                        description: 'Заполните все поля анкеты. Статус заявки можно отследить на сайте и в приложении'
                    },
                    en: {
                        title: 'Fill in the questionnaire',
                        description: 'Please fill in all the fields of a questionnaire. You may track the status of your request through the website and application'
                    },
                    fa: {
                        title: 'فرم درخواست را ﺗﮑﻤﻴﻞ ﮐﻨﻴﺪ',
                        description: 'همه فیلد های فرم درخواست را ﺗﮑﻤﻴﻞ کنید. وضعیت درخواست را می توان در سایت و نرم افزار پیگیری کرد'
                    },
                    ar: {
                        title: 'قم بتعبئة الطلب',
                        description: 'قم بتعبئة جميع بنود الطلب. يمكن تتبع حالة الطلب على الموقع وفي التطبيق.'
                    },
                    img: '02'
                },
                {
                    ru: {
                        title: 'Получите карту',
                        description: 'Карта будет доставлена по адресу, указанному в анкете. Доступен самовывоз из офисов партнеров'
                    },
                    en: {
                        title: 'Get a card',
                        description: 'Your card will be delivered to the address indicated in the questionnaire<br/><br/>You can pick up your card from partner offices'
                    },
                    fa: {
                        title: 'دریافت کارت',
                        description: 'کارت به آدرسی که در فرم درخواست مشخص شده بود، تحویل خواهد شد<br/><br/>شما می توانید خودتان کارت را در دفتر شریک دریافت کنید'
                    },
                    ar: {
                        title: 'إحصل على بطاقة',
                        description: 'سيتم تسليم البطاقة إلى العنوان المحدد في الطلب <br/><br/>متاح إستلامها من مكاتب الشركاء'
                    },
                    img: '04'
                }
            ]
        }
    },
};
