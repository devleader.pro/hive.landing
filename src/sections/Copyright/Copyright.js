export default {
    name: 'Copyright',
    data: function () {
        return {
            copyright: {
                ru: `© 2019 Сервис предоставляется при технической поддержке Процессингового Центра ООО «Рукард»`,
                en: '© 2019 Service is provided with the technical support of the Processing Center LLC Rukard',
                fa: `2019 © این سرویس با پشتیبانی فنی مرکز پردازش شرکت با مسئولیت محدود  "Rucard" ارائه می گردد`,
                ar: `© 2019 يتم تقديم هذه الخدمة بدعم تقني من مركز المعالجة " روكارد "`,
            },
        }
    },

};
