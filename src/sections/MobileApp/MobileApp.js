export default {
    name: 'MobileApp',
    data: function () {
        return {
            nameSection: {
                ru: 'Многофункциональный мобильный банк',
                en: 'Multi-functional mobile banking',
                fa: 'موبایل بانک چند منظوره',
                ar: 'بنك المحمول المتعدد الوظائف'
            },
            capsone: [{
                    ru: {
                        title: 'Переводы',
                        description: 'Переводите деньги между своими картами, друзьям или близким'
                    },
                    en: {
                        title: 'Transfers',
                        description: 'Transfer money between your cards to friends and family'
                    },
                    fa: {
                        title: 'انتقالات',
                        description: 'انتقال پول بین کارت های خود، به دوستان و خانواده'
                    },
                    ar: {
                        title: 'التحويلات',
                        description: 'قم بتحويل الأموال بين بطاقاتك أو الأصدقاء أو الأقارب'
                    },
                    img: 'collapse',
                    screen: 'transfers'
                },
                {
                    ru: {
                        title: 'Информация по счёту',
                        description: 'Контролируйте остаток средств на вашем счете в режиме online'
                    },
                    en: {
                        title: 'Account information ',
                        description: 'Find out how much is left on your account, and what is your last transaction'
                    },
                    fa: {
                        title: 'اطلاعات حساب',
                        description: 'شما می توانید موجودی وجوه حسابتان را به طور آنلاین مدیریت کنید'
                    },
                    ar: {
                        title: 'معلومات الحساب',
                        description: 'راقب الأموال المتبقية على رصيد حسابك في حالة online'
                    },
                    img: 'upload',
                    screen: 'information'
                },
                {
                    ru: {
                        title: 'Расходы',
                        description: 'Получайте полную информацию обо всех операциях по вашим картам'
                    },
                    en: {
                        title: 'Expenses ',
                        description: 'Get full information on all card transactions'
                    },
                    fa: {
                        title: 'هزینه ها',
                        description: 'می توانید با کارتتان از همه ی تراکنش ها آگاه شوید. '
                    },
                    ar: {
                        title: 'النفقات',
                        description: 'إحصل على معلومات كاملة عن جميع العمليات على بطاقاتك'
                    },
                    img: 'pay',
                    screen: 'expenses'
                }
            ],
            capstwo: [{
                    ru: {
                        title: 'Чат с банком',
                        description: 'Общайтесь с банком в онлайн чате прямо в приложении'
                    },
                    en: {
                        title: 'Chat with bank',
                        description: 'Communicate with the bank in an online chat in the app'
                    },
                    fa: {
                        title: 'چت با بانک ',
                        description: 'با بانک در چت آنلاین نرم افزار مشاوره کنید'
                    },
                    ar: {
                        title: 'الدردشة مع البنك',
                        description: 'دردش مع البنك في الدردشة عبر الإنترنت مباشرة في التطبيق'
                    },
                    img: 'chat',
                    screen: 'chat'
                },
                {
                    ru: {
                        title: 'Выписки по счёту',
                        description: 'Заказывайте выписки по карте за любой период. Получайте их на почту в pdf'
                    },
                    en: {
                        title: 'Account statements',
                        description: 'Order card statements for any period. Receive it by e-mail in pdf'
                    },
                    fa: {
                        title: 'صورتحساب ها',
                        description: 'می توانید صورتحساب های کارت را برای هر دوره سفارش دهید و آن را از طریق پست الکترونیکی به صورت pdf دریافت کنید'
                    },
                    ar: {
                        title: 'كشوف الحساب',
                        description: 'إحجز كشوف البطاقة لأي فترة . و إحصل على البريد في pdf'
                    },
                    img: 'mixer',
                    screen: 'statements'
                },
                {
                    ru: {
                        title: 'Лимиты',
                        description: 'Управляйте количеством денежных средств на снятие и покупки в день и месяц'
                    },
                    en: {
                        title: 'Limits ',
                        description: 'Manage the amount of cash for withdrawals and purchases per day and month'
                    },
                    fa: {
                        title: 'محدودیت',
                        description: 'مقدار پول نقد را برای برداشت و خرید روزانه و ماهانه مدیریت کنید'
                    },
                    ar: {
                        title: 'الحدود',
                        description: 'قم بإدارة السحوبات النقدية والمشتريات في اليوم والشهر'
                    },
                    img: 'shield',
                    screen: 'limits'
                }
            ],
            activeScreen: 'transfers'
        }
    },
};
