//import Plugins
import "babel-polyfill";
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { HTTP as axios } from './axios/axios';
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import en from 'vuejs-datepicker/dist/locale/translations/en.js'
import fa from 'vuejs-datepicker/dist/locale/translations/fa.js'
import ru from 'vuejs-datepicker/dist/locale/translations/ru.js'
import ar from 'vuejs-datepicker/dist/locale/translations/ar.js'
const VueInputMask = require('vue-inputmask').default
import Datepicker from 'vuejs-datepicker';
import VueFlatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';
Vue.use(VueInputMask)

Vue.component('v-select', vSelect)
Vue.prototype.$http = axios;

Vue.use( Vuex );
Vue.use( VueRouter );
Vue.config.debug = true;
Vue.config.devTools = true;
Vue.component( 'datepicker', Datepicker );
Vue.use(VueFlatPickr);


import App from './App.vue';
Vue.component( 'App', App );
import Home from './pages/Home/Home.vue';
Vue.component( 'Home', Home );

//Routers
const router = new VueRouter( {
	mode: 'history',
	routes: [
		{ path: '/', name: 'Home', component: Home }, 	
	],
	scrollBehavior (to, from, savedPosition) {
		if(to.hash){
			return { selector: to.hash }
		} else {
			return { x: 0, y: 0 }
		}
	}
   
} );

//Define rtl/ltr variable
Object.defineProperty(Vue.prototype, '$lang', {
    get: function () {
        return this.$root.lang;
    },
    set: function(lang) {
        this.$root.lang = lang;
    }
});

//Define vuex store
const store = new Vuex.Store( {
	state: {
		title: 'Hive'
	},
	mutations: {
		rtChangeTitle( state, value ) {
			state.title = value;
			document.title = ( state.title ? state.title + ' - ' : '' ) + 'HiveCard.Com';
		}
	}
} );

// Create instance of main component
new Vue( {
	router,
	store,
	render: createElement => createElement( App ),
    beforeMount() {
	    if (window.localStorage.lang === 'fa' || window.localStorage.lang === 'ar') {
	        this.rtl = true;
        }
    },
	data() {
		return {
			rtl: false,
			lang: 'ru',
			modals: {
				openned: false,
				component: '',
				body: {},
			},
			calendarLangs:{
				en: en,
				ru: ru,
				fa: fa,
				ar: ar,
			},
			selectedCard: {},
            langReady: true,
		};
	},
	watch:{
		["modals.openned"]() {
				document.getElementsByTagName("html")[0].style = `overflow-y: ${this.modals.openned === true  ? 'hidden' : 'auto' }`
		}
	},
	methods: {
		openModal(name, requestFunc, body){
			this.modals = {
				openned: true,
				component: name,
				body: body ? body : {},
                requestFunc: requestFunc
			}
		}
	},
	
	
} ).$mount( '#app' );
