export default {
	methods: {
		check( attr ){
			for (let i = 0; i < attr.length; i++) {
				let name = attr[i]
				if ( !this[name] ) {
					this.disabled = true
					return
				}
			}
			this.disabled = false
		}
	}
}; 
