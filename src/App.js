import Header from "@sections/Header/Header.vue"
import Modals from "@components/Modals/Modals.vue"

export default {
    created: function () {
        let langs = ['ru', 'en', 'fa', 'ar']
        if (!window.localStorage.lang || langs.indexOf(window.localStorage.lang.substr(0, 2).toLowerCase()) === -1) {

            let language = window.navigator ? window.navigator.language  : "en";

            language = language.substr(0, 2).toLowerCase();
            this.$root.lang = language;
        } else {
            this.$root.lang = window.localStorage.lang;
        }
    },
    components: {
        Header,
        Modals
    },
    mixins: [],
    data: function () {
        return {

        }
    },
    watch: {
        $lang(newLangValue) {
            localStorage.lang = newLangValue;
        }
    }
};
