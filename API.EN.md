# API "HIVECARD"  
## Urls  
development: "//api.ics.rusoft.pro", 
production: "//api.inkarobank.tech",  
We put this url in `process.env.APPLICATION_API`   
## Telephone number validation  
Here we check, if the number is not busy. 
```javascript 
import { stringify } from 'query-string'  
fetch(process.env.APPLICATION_API + '/api/phone/validate?' + stringify({ 
        name: this.state[FieldNames.FIRST_NAME], 
        fathersName: this.state[FieldNames.MIDDLE_NAME], 
        surname: this.state[FieldNames.LAST_NAME], 
        phone: this.state[FieldNames.PHONE], 
        })) 
```  
## Request for SMS-code  
```javascript 
api.post('/api/phone', {phone: this.state[FieldNames.PHONE]}) 
.then(response => response.identifier) 
```  
An SMS will be received on the phone, while the identifier will be needed for the next request. 
*`api.post` и `api.get` - these are wrappers for fetch, they will also appear later.*  
## Number confirmation  
```javascript 
api.post('/api/phone/verify', { 
    code: this.state.SMScode, 
    identifier: this.state.identifier, 
}) 
```  
A positive response to this request means that the number is confirmed  
## Sending of application form  
The server receives only valid data, in case of error it does not send back any information. 
The validation is performed on the client.  
### Request structure  
```javascript 
fetch(process.env.APPLICATION_API + '/api/application', { 
    body: formData, method: 'POST', 
}) 
```  
where  
```javascript 
const formData = new FormData() 
formData.append('files', this.state.files[0]) // passport scan 
formData.append("application", new Blob([JSON.stringify(formJson)], { type: 'application/json', })) // application form 
```  
where  
```javascript 
const formJson = { 
    domicile: this.state[FieldNames.DOMICILE], 
    surname: this.state[FieldNames.LAST_NAME], 
    name: this.state[FieldNames.FIRST_NAME], 
    fathersName: this.state[FieldNames.MIDDLE_NAME], 
    dateOfBirth: this.state[FieldNames.DATE_OF_BIRTH], 
    placeOfBirth: this.state[FieldNames.PLACE_OF_BIRTH], 
    gender: this.state[FieldNames.GENDER].id, 
    passportSerialNumber: this.state[FieldNames.PASSPORT_SERIAL_NUMBER], 
    dateOfIssue: this.state[FieldNames.DATE_OF_ISSUE], 
    dateOfExpire: this.state[FieldNames.DATE_OF_EXPIRE], 
    placeOfIssue: this.state[FieldNames.PLACE_OF_ISSUE], 
    issuingOfficerName: this.state[FieldNames.NAME_OF_ISSUING_OFFICER], 
    issuingOfficerPosition: this.state[FieldNames.POSITION_OF_ISSUING_OFFICER],  
    operCntPlan: this.state[FieldNames.PLANNED_OPERATIONS_COUNTS].id, 
    operAmtPlan: this.state[FieldNames.PLANNED_OPERATIONS_AMOUNT].id, 
    moneySources: this.state[FieldNames.FINANCIALSOURCES], 
    activity: this.state[FieldNames.FIELD_OF_ACTIVITY].id, 
    position: this.state[FieldNames.POSITION_HELD].id, 
    workExperience: this.state[FieldNames.WORK_EXPERIENCE].id, 
    isProprietor: this.state[FieldNames.HAS_REAL_PROPERTY], 
    properties: this.state[FieldNames.REAL_PROPERTY], 
    vehicleCost: this.state[FieldNames.COST_OF_REAL_PROPERTY].id, 
    email: this.state[FieldNames.EMAIL], 
    codeword: this.state[FieldNames.CODEWORD], 
    phone: this.state[FieldNames.PHONE], 
    smsCode: this.state[FieldNames.PHONE_CONFIRMATION_CODE], 
    phoneVerificationIdentifier: this.state.phoneIdentifier, 
    promo: this.state[FieldNames.PROMOCODE],  
    homeAddress: { 
        country: this.state[FieldNames.COUNTRY], 
        administrativeAreaId: this.state[FieldNames.ADMINISTRATIVE_AREA].id, 
        subAdministrativeAreaId: this.state[FieldNames.SUB_ADMINISTRATIVE_AREA].id, 
        settlementTypeId: this.state[FieldNames.SETTLEMENT_TYPE].id, 
        settlementName: this.state[FieldNames.SETTLEMENT_NAME], 
        square: this.state[FieldNames.SQUARE], 
        mainStreet: this.state[FieldNames.MAIN_STREET], 
        minorStreet: this.state[FieldNames.MINOR_STREET], 
        lane: this.state[FieldNames.LANE], 
        building: this.state[FieldNames.BUILDING], 
        apartment: this.state[FieldNames.APARTMENT], 
        postalCode: this.state[FieldNames.IDENTIFICATION_NUMBER], 
    }, 
    agencyId: this.state[FieldNames.AGENCY].id, 
    deliveryMethod: 'OFFICE', 
    productCode: this.getCardType(), 
    isRelative: false,  
    relativeType: null,  
}; 
```  
#### Date format: 
String format `YYYY-MM-DD`  
#### The fields are filled in with the values from vocabularies 
- gender 
- operCntPlan 
- operAmtPlan 
- activity 
- position 
- workExperience 
- vehicleCost 
- administrativeAreaId 
- subAdministrativeAreaId 
- settlementTypeId 
- agencyId  
#### Card type (productCode) 
The `productCode` field can contain one of the following values:  
- CLASSIC 
- GOLD 
- PLATINUM  
#### Fields in Farsi 
- settlementName 
- domicile  
### Additional information on fields’ validation  
- Country - always , ایران disabled 
- Province – selection from the list of provinces sorted alphabetically, obligatory 
- Region – selection from the list of regions sorted alphabetically, provided a particular one is selected, disabled till a province is selected, obligatory  
- Locality type – Selection from the list of Locality types, obligatory 
- Locality name – Input format: entry in Farsi, gaps, dashes, maximum 100, obligatory 
- Square - Input format: entry in Farsi, numbers -> Persian numbers (- > hereinafter means that, if you insert Arabic numbers they will be automatically switched to the Persian ones), Persian numbers, gaps, dashes, maximum 100 
- Main street - Input format: entry in Farsi, numbers -> Persian numbers, Persian numbers, gaps, dashes, maximum 100 
- Secondary street - Input format: entry in Farsi, numbers -> Persian numbers, Persian numbers, gaps, dashes, maximum 100 
- Side street - Input format: entry in Farsi, numbers -> Persian numbers, Persian numbers, gaps, dashes, maximum 100 
- Number or name of the building - Input format: entry in Farsi, numbers -> Persian numbers, Persian numbers, gaps, dashes, maximum 50 
- Apartment number - Input format: numbers -> Persian numbers, Persian numbers, gaps, dashes, maximum 5 
- Postal code - Input format: numbers -> Persian numbers, Persian numbers, gaps, dashes, maximum 11, obligatory 
- Nationality - IR by default, Input format: entry in English (Abcd), limited to 2 characters, obligatory 
- Surname – Input format: entry in English (Abcd), obligatory  
- Name - Input format: entry in English (Abcd), obligatory 
- Middle name - Input format: entry in English (Abcd), obligatory 
- Date of birth – Input format: international date format: 01/01/1910, obligatory
- Place of birth – Input format: entry in English (Abcd), gaps, dashes, maximum 250, obligatory  
- Sex - Male / Female, obligatory 
- Passport number – the first character is English letter, the others are numbers, gaps, dashes, obligatory   
- Passport issue date – Input format: international date format: 01/01/1910, obligatory 
- Passport validity period end date - Input format: international date format: 01/01/1910, obligatory 
- Place of passport issue – Input format: entry in English (Abcd), gaps, dashes, maximum 250, obligatory  
- Name of the person who issued the passport – Issue format: entry in English (Abcd), gaps, dashes, maximum 100, obligatory 
- Position of the person who issued the passport – Issue format: entry in English (Abcd), gaps, dashes, maximum 100, obligatory 
- E-mail - maximum 100 
- Code word – only Latin letters, maximum 10, obligatory  
## Receiving data from vocabularies  
For all fields where the data from vocabularies is used, except `subAdministrativeAreaId`, this request must be used:  
```javascript 
api.get(`https://dict.inkarobank.tech/api/v1/dictionary/${dictionary}`) 
```  
Where  `dictionary` - one of these options: 
- activity 
- adminarea 
- agency 
- moneysources 
- numplanoperations 
- ownerproperties 
- position 
- settlementtype 
- sumplanoperations 
- vehiclecost 
- workexperience  
### Receiving data from vocabulary for  subAdministrativeAreaId  
```javascript 
api.get(`https://dict.inkarobank.tech/api/v1/dictionary/adminarea/${id}`) 
```  
Where `id` - id of the selected element from the field administrativeAreaId  
## Checking application status  
```javascript 
api.post('/api/application/state/list', { 
    orderNumber: this.state.orderNumber, 
    birthday: this.state.birthday, 
}) 
```   
Where `orderNumber` - application number 

