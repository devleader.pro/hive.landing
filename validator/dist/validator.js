'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _getIterator2 = require('babel-runtime/core-js/get-iterator');

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _email = require('./email');

var _email2 = _interopRequireDefault(_email);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Validator = function Validator() {
    var _this = this;

    var newValidators = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    (0, _classCallCheck3.default)(this, Validator);
    this.validators = {
        onlyDigits: {
            validate: function validate(text) {
                return text.search(/^\d*$/) === 0;
            },
            message: 'Может содержать только цифры'
        },
        onlyEnglishLetters: {
            validate: function validate(text) {
                return text.search(/^[a-z]*$/i) === 0;
            },
            message: 'Может содержать только латинские буквы'
        },
        onlyEnglishLettersAndDigits: {
            validate: function validate(text) {
                return text.search(/^[a-z\d]*$/i) === 0;
            },
            message: 'Может содержать только латинские буквы и цифры'
        },
        notEmpty: {
            validate: function validate(text) {
                return Boolean(text.length);
            },
            message: 'Не может быть пустой строкой'
        },
        email: {
            validate: function validate(text) {
                return (0, _email2.default)(text);
            },
            message: 'Ожидается корректный адрес электронной почты'
        },
        length: function length() {
            var _length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

            return {
                validate: function validate() {
                    var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
                    return text.length == _length;
                },
                message: '\u0414\u043B\u0438\u043D\u0430 \u0441\u0442\u0440\u043E\u043A\u0438 \u0434\u043E\u043B\u0436\u043D\u0430 \u0431\u044B\u0442\u044C \u0440\u0430\u0432\u043D\u0430 ' + _length
            };
        },
        minLength: function minLength() {
            var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            return {
                validate: function validate() {
                    var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
                    return text.length >= length;
                },
                message: '\u0414\u043E\u043B\u0436\u043D\u043E \u0431\u044B\u0442\u044C \u043D\u0435 \u043C\u0435\u043D\u044C\u0448\u0435 ' + length
            };
        },
        maxLength: function maxLength() {
            var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
            return {
                validate: function validate() {
                    var text = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
                    return text.length <= length;
                },
                message: '\u0414\u043E\u043B\u0436\u043D\u043E \u0431\u044B\u0442\u044C \u043D\u0435 \u0431\u043E\u043B\u044C\u0448\u0435 ' + length
            };
        }
    };

    this.getErrorMsg = function () {
        for (var _len = arguments.length, validators = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            validators[_key - 1] = arguments[_key];
        }

        var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        var messages = _this.getErrorMsgs.apply(_this, [value].concat(validators));
        return messages.length > 0 ? messages[0] : '';
    };

    this.getErrorMsgs = function () {
        for (var _len2 = arguments.length, validators = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
            validators[_key2 - 1] = arguments[_key2];
        }

        var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

        var missingValidator = validators.find(function (v) {
            return !_this.validators[Array.isArray(v) ? v[0] : v];
        });
        if (missingValidator) {
            throw new TypeError('Validator ' + missingValidator + ' is missing');
        }
        var result = [];
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = (0, _getIterator3.default)(validators), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var v = _step.value;

                var isArray = Array.isArray(v);
                var validator = isArray ? _this.validators[v[0]](v[1]) : _this.validators[v];
                if (!validator.validate(value)) {
                    result.push(validator.message);
                }
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        return result;
    };

    this.isValid = function () {
        for (var _len3 = arguments.length, validators = Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
            validators[_key3 - 1] = arguments[_key3];
        }

        var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
        return !_this.getErrorMsgs.apply(_this, [value].concat(validators)).length;
    };

    for (var name in newValidators) {
        var validator = typeof newValidators[name] == 'function' ? newValidators[name]() : newValidators[name];
        if (typeof validator.validate != 'function' || typeof validator.message != 'string') {
            throw new TypeError('Validator ' + name + ' is wrong. There is no validate function or a wrong message.');
        }
    }
    this.validators = (0, _extends3.default)({}, this.validators, newValidators);
};

exports.default = Validator;
//# sourceMappingURL=validator.js.map