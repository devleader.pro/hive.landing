'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _validator = require('./validator');

var _validator2 = _interopRequireDefault(_validator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _validator2.default;
//# sourceMappingURL=index.js.map