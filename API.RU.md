# API "HIVECARD"

## Urls

development: "//api.ics.rusoft.pro",
production: "//api.inkarobank.tech",

Мы кладем этот url в `process.env.APPLICATION_API`


## Валидация телефона

Здесь мы проверяем не занят ли номер телефона

```javascript
import { stringify } from 'query-string'

fetch(process.env.APPLICATION_API + '/api/phone/validate?' + stringify({
        name: this.state[FieldNames.FIRST_NAME],
        fathersName: this.state[FieldNames.MIDDLE_NAME],
        surname: this.state[FieldNames.LAST_NAME],
        phone: this.state[FieldNames.PHONE],
    }))
```

## Запрос СМС-кода

```javascript
api.post('/api/phone', {phone: this.state[FieldNames.PHONE]})
.then(response => response.identifier)
```

На телефон придет смс, а идентификатор пригодится для следующего запроса

*`api.post` и `api.get` - это обертки над fetch, будет встречаться и далее.*

## Подтверждение номера

```javascript
api.post('/api/phone/verify', {
    code: this.state.SMScode,
    identifier: this.state.identifier,
})
```

После получения положительного ответа на этот запрос, считаем телефон подтвержденным

## Отправка формы заявки

Сервер принимает только валидные данные, в случае ошибки он не возвращает никакой информации.
Вся валидация реализуется на клиенте.

### Структура запроса

```javascript
fetch(process.env.APPLICATION_API + '/api/application', {
    body: formData, method: 'POST',
})
```

где 

```javascript
const formData = new FormData()
formData.append('files', this.state.files[0]) // скан паспорта
formData.append("application", new Blob([JSON.stringify(formJson)], { type: 'application/json', })) // форма заявки
```

где 

```javascript
const formJson = {
    domicile: this.state[FieldNames.DOMICILE],
    surname: this.state[FieldNames.LAST_NAME],
    name: this.state[FieldNames.FIRST_NAME],
    fathersName: this.state[FieldNames.MIDDLE_NAME],
    dateOfBirth: this.state[FieldNames.DATE_OF_BIRTH],
    placeOfBirth: this.state[FieldNames.PLACE_OF_BIRTH],
    gender: this.state[FieldNames.GENDER].id,
    passportSerialNumber: this.state[FieldNames.PASSPORT_SERIAL_NUMBER],
    dateOfIssue: this.state[FieldNames.DATE_OF_ISSUE],
    dateOfExpire: this.state[FieldNames.DATE_OF_EXPIRE],
    placeOfIssue: this.state[FieldNames.PLACE_OF_ISSUE],
    issuingOfficerName: this.state[FieldNames.NAME_OF_ISSUING_OFFICER],
    issuingOfficerPosition: this.state[FieldNames.POSITION_OF_ISSUING_OFFICER],

    operCntPlan: this.state[FieldNames.PLANNED_OPERATIONS_COUNTS].id,
    operAmtPlan: this.state[FieldNames.PLANNED_OPERATIONS_AMOUNT].id,
    moneySources: this.state[FieldNames.FINANCIALSOURCES],
    activity: this.state[FieldNames.FIELD_OF_ACTIVITY].id,
    position: this.state[FieldNames.POSITION_HELD].id,
    workExperience: this.state[FieldNames.WORK_EXPERIENCE].id,
    isProprietor: this.state[FieldNames.HAS_REAL_PROPERTY],
    properties: this.state[FieldNames.REAL_PROPERTY],
    vehicleCost: this.state[FieldNames.COST_OF_REAL_PROPERTY].id,
    email: this.state[FieldNames.EMAIL],
    codeword: this.state[FieldNames.CODEWORD],
    phone: this.state[FieldNames.PHONE],
    smsCode: this.state[FieldNames.PHONE_CONFIRMATION_CODE],
    phoneVerificationIdentifier: this.state.phoneIdentifier,
    promo: this.state[FieldNames.PROMOCODE],

    homeAddress: {
        country: this.state[FieldNames.COUNTRY],
        administrativeAreaId: this.state[FieldNames.ADMINISTRATIVE_AREA].id,
        subAdministrativeAreaId: this.state[FieldNames.SUB_ADMINISTRATIVE_AREA].id,
        settlementTypeId: this.state[FieldNames.SETTLEMENT_TYPE].id,
        settlementName: this.state[FieldNames.SETTLEMENT_NAME],
        square: this.state[FieldNames.SQUARE],
        mainStreet: this.state[FieldNames.MAIN_STREET],
        minorStreet: this.state[FieldNames.MINOR_STREET],
        lane: this.state[FieldNames.LANE],
        building: this.state[FieldNames.BUILDING],
        apartment: this.state[FieldNames.APARTMENT],
        postalCode: this.state[FieldNames.IDENTIFICATION_NUMBER],
    },
    agencyId: this.state[FieldNames.AGENCY].id,
    deliveryMethod: 'OFFICE',
    productCode: this.getCardType(),
    isRelative: false, 
    relativeType: null, 
};
```

#### Формат дат:
строка вида `YYYY-MM-DD`

#### Поля заполняются значениями из словарей
- gender
- operCntPlan
- operAmtPlan
- activity
- position
- workExperience
- vehicleCost
- administrativeAreaId
- subAdministrativeAreaId
- settlementTypeId
- agencyId

#### Тип карты (productCode)
Поле `productCode` может содержать один из вариантов: 
- CLASSIC
- GOLD
- PLATINUM

#### Поля на фарси
- settlementName
- domicile

### Дополнительная информация по валидации полей

- Страна - всегда ایران, disabled
- Провинция - выбор из списка провинций, отсортированных по алфавиту, обязательное
- Область - выбор из списка областей, отсортированных по алфавиту, при условии выбора конкретной области, disabled, пока не выбрана провинция, обязательное
- Вид населенного пункта - Выбор из списка Вид населенного пункта, обязательное
- Название населенного пункта - Формат ввода: ввод на персидском языке, пробелы, черточки, макс 100, обязательное
- Площадь - Формат ввода: ввод на персидском языке, цифры -> персидские цифры, персидские цифры, пробелы, черточки, макс 100
- Главная улица - Формат ввода: ввод на персидском языке, цифры -> персидские цифры, персидские цифры, пробелы, черточки, макс 100
- Второстепенная улица - Формат ввода: ввод на персидском языке, цифры -> персидские цифры, персидские цифры, пробелы, черточки, макс 100
- Переулок - Формат ввода: ввод на персидском языке, цифры -> персидские цифры, персидские цифры, пробелы, черточки, макс 100
- Номер или название дома - Формат ввода: ввод на персидском языке, цифры -> персидские цифры, персидские цифры, пробелы, черточки, макс 50
- Номер квартиры - Формат ввода: цифры -> персидские цифры, персидские цифры, пробелы, черточки, макс 5
- Почтовый код - Формат ввода: цифры -> персидские цифры, персидские цифры, пробелы, черточки, макс 11, обязательное
- Гражданство - по умолчанию IR, Формат ввода: ввод на английском языке (Abcd), строго 2 символа, обязательное
- Фамилия - Формат ввода: ввод на английском языке (Abcd), обязательное 
- Имя - Формат ввода: ввод на английском языке (Abcd), обязательное
- Отчество - Формат ввода: ввод на английском языке (Abcd), обязательное
- Дата рождения - Формат ввода: ввод даты в международном формате: 01/01/1910, обязательное
- Место рождения - Формат ввода: ввод на английском языке (Abcd), пробелы, черточки, макс 250, обязательное 
- Пол - Male / Female, обязательное
- Номер паспорта - 1-ая английская буква, остальные цифры, пробелы, черточки, обязательное
- Дата выдачи паспорта - Формат ввода: ввод даты в международном формате: 01/01/1910, обязательное
- Дата окончания действия паспорта - Формат ввода: ввод даты в международном формате: 01/01/1910, обязательное
- Место выдачи паспорта - Формат ввода: ввод на английском языке (Abcd), пробелы, черточки, макс 250, обязательное 
- Имя выдавшего паспорт - Формат ввода: ввод на английском языке (Abcd), пробелы, черточки, макс 100, обязательное 
- Должность выдавшего паспорт - Формат ввода: ввод на английском языке (Abcd), пробелы, черточки, макс 100, обязательное
- E-mail - макс 100
- Кодовое слово - только латинские буквы, макс 10, обязательное

## Получение данных из словарей

Для всех полей, где используются данные из словарей, кроме `subAdministrativeAreaId`, нужно использовать запрос

```javascript
api.get(`https://dict.inkarobank.tech/api/v1/dictionary/${dictionary}`)
```

где `dictionary` - один из следующих вариантов:
- activity
- adminarea
- agency
- moneysources
- numplanoperations
- ownerproperties
- position
- settlementtype
- sumplanoperations
- vehiclecost
- workexperience

### Получение данных из словаря для subAdministrativeAreaId

```javascript
api.get(`https://dict.inkarobank.tech/api/v1/dictionary/adminarea/${id}`)
```

где `id` - id выбранного элемента из поля administrativeAreaId

## Проверка статуса заявки

```javascript
api.post('/api/application/state/list', {
    orderNumber: this.state.orderNumber,
    birthday: this.state.birthday,
})
``` 

где `orderNumber` - номер заявки
